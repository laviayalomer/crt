﻿using System;
using System.Collections.Generic;
using System.Text;


namespace RadixTree
{
    public class MyStringBuilder<T>
    {
        static string nodeSign = ((char)0x0298).ToString();
        static string TAB = "    ";
        static string FLOOR_DOWN = "│   ";
        static string BOTTOM_NODE_REPRESENTATION = "└── " + nodeSign + " ";
        static string COMMON_SIBLING_NODE_REPRESENTATION = "├── " + nodeSign + " ";

        public static String BuildStringFromTree(CRT<T> tree)
        {
            StringBuilder sb = new StringBuilder();
            BuildStringFromTree(tree.root, sb, "", true, true);
            return sb.ToString();
        }

        public static void BuildStringFromTree(Node<T> node, StringBuilder sb, String prefix, bool isBottomSibling, bool isSubTree)
        {
            StringBuilder treeRepresentation = new StringBuilder();
            if (isSubTree)
            {
                treeRepresentation.Append(nodeSign);
            }

            treeRepresentation.Append(node.GetIncomingEdges());
            if (!node.GetValue().Equals(default(T)))
            {
                treeRepresentation = DrawNodeWithValue(treeRepresentation, node);
            }

            sb.Append(prefix);
            string nextConcatenation = "";

            if (isBottomSibling)
            {
                if (!isSubTree)
                {
                    nextConcatenation = BOTTOM_NODE_REPRESENTATION;
                }
            }
            else
            {
                nextConcatenation = COMMON_SIBLING_NODE_REPRESENTATION;
            }

            sb.Append(nextConcatenation).Append(treeRepresentation).Append("\n");

            List<Node<T>> children = node.GetOutGoingEdgesList();
            for (int i = 0; i < children.Size() - 1; i++)
            {
                String nextRecursivePrefix = prefix;
                if (isBottomSibling)
                {
                    if (!isSubTree)
                    {
                        nextRecursivePrefix += TAB;
                    }
                }
                else
                {
                    nextRecursivePrefix += FLOOR_DOWN;
                }

                BuildStringFromTree(children.GetIndex(i), sb, nextRecursivePrefix, false, false);
            }

            if (children.Size() != 0)
            {
                String nextRecursivePrefix = prefix;
                if (isBottomSibling)
                {
                    if (!isSubTree)
                    {
                        nextRecursivePrefix += TAB;
                    }
                }
                else
                {
                    nextRecursivePrefix += FLOOR_DOWN;
                }

                BuildStringFromTree(children.GetIndex(children.Size() - 1), sb, nextRecursivePrefix, true, false);
            }
        }

        public static StringBuilder DrawNodeWithValue(StringBuilder treeRepresentation, Node<T> node)
        {
            StringBuilder valueNodeSb = new StringBuilder();
            valueNodeSb.Append("[").Append(treeRepresentation.ToString()).Append(", ").Append(node.GetValue()).Append("]");
            return valueNodeSb;
        }
    }
}
