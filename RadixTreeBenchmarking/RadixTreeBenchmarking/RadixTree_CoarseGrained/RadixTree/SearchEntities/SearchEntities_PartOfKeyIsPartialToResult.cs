﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RadixTree_CoarseGrained
{
    public class SearchEntities_PartOfKeyIsPartialToResult<T> : SearchEntities<T>
    {

        public SearchEntities_PartOfKeyIsPartialToResult(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {
        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<string>();
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {            
            string keyOfParentNode = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars - numOfEqualNodeChars);
            string keyOfNodeFound = RadixTreeUtils<T>.Concatenate(keyOfParentNode, resultNode.GetIncomingEdges()).ToString();
            return currentTree.SearchChildernKeys(keyOfNodeFound, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
        }
        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {            
            string keyOfParentNode = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars - numOfEqualNodeChars);
            string keyOfNodeFound = RadixTreeUtils<T>.Concatenate(keyOfParentNode, resultNode.GetIncomingEdges()).ToString();
            return currentTree.SearchChildernKeyValuePairs(keyOfNodeFound, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return default(T);
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<T>();
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {            
            string keyOfParentNode = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars - numOfEqualNodeChars);
            string keyOfNodeFound = RadixTreeUtils<T>.Concatenate(keyOfParentNode, resultNode.GetIncomingEdges()).ToString();
            return currentTree.SearchChildrenValues(keyOfNodeFound, resultNode);
        }

        public override ValidationState InsertSearchEntities(bool overwrite, T value, ref T res)
        {                      
            string keyCharsFromStartOfNodeFound = key.Substring(totalNumOfEqualChars - numOfEqualNodeChars);
            string commonPrefix = RadixTreeUtils<T>.GetCommonPrefix(keyCharsFromStartOfNodeFound, resultNode.GetIncomingEdges());
            string suffixFromExistingEdge = RadixTreeUtils<T>.RemovePrefix(resultNode.GetIncomingEdges(), commonPrefix);
            string suffixFromKey = key.Substring(totalNumOfEqualChars);

            List<Node<T>> newl = new List<Node<T>>();
            Node<T> keySuffixNode = new Node<T>(value, suffixFromKey, new List<Node<T>>(), false);
            Node<T> edgeSuffixNode = new Node<T>(resultNode.GetValue(), suffixFromExistingEdge, resultNode.GetOutGoingEdgesList(), false);     
            newl.Add(keySuffixNode);
            newl.Add(edgeSuffixNode);
            Node<T> prefixNode = new Node<T>(default(T), commonPrefix, newl, false);
            parent.UpdateOutEdgeByChildNode(prefixNode);
            res = value;
            if (res.Equals(default(T)))
            {
                Console.WriteLine("Break");
            }
            return ValidationState.SUCCESS;
        }
		
        public override ValidationState RemoveSearchEntities()
        {
            return ValidationState.FAILURE;
        }
    }
}
