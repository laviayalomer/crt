﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RadixTree_FineGrained
{
    public class SearchEntities_KeyFound<T> : SearchEntities<T>
    {
        public SearchEntities_KeyFound(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {
        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            return currentTree.SearchChildernKeys(key, resultNode);
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            return currentTree.SearchChildernKeys(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            return currentTree.SearchChildernKeyValuePairs(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {
            return currentTree.SearchChildernKeyValuePairs(key, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return resultNode.GetValue();
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            return currentTree.SearchChildrenValues(key, resultNode);
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {
            return currentTree.SearchChildrenValues(key, resultNode);
        }

        public override ValidationState InsertSearchEntities(bool overwrite, T value, ref T res)
        {
            //Monitor.Enter(currentTree.locker);
            if (!((grandParent == null ? true : Monitor.TryEnter(grandParent)) && (parent == null ? true : Monitor.TryEnter(parent)) && Monitor.TryEnter(resultNode)))
            {
                ReleaseAllI();
                return ValidationState.KEEP_GOING;
            }
            //Monitor.Exit(currentTree.locker);

            ValidationState validate = ValidateInsert();
            if (!validate.Equals(ValidationState.SUCCESS))
            {
                ReleaseAllI();
                return validate;
            }

            res = resultNode.GetValue();
            if (!overwrite && !res.Equals(default(T)))
            {
                ReleaseAllI();
                return ValidationState.SUCCESS;
            }
            
            Node<T> updatedNode = new Node<T>(value, resultNode.GetIncomingEdges(), resultNode.GetOutGoingEdgesList(), false);
            parent.UpdateOutEdgeByChildNode(updatedNode);

            ReleaseAllI();
            return ValidationState.SUCCESS;
        }

        public override ValidationState RemoveSearchEntities()
        {
            if (!((grandParent == null ? true : Monitor.TryEnter(grandParent)) && (parent == null ? true : Monitor.TryEnter(parent)) && Monitor.TryEnter(resultNode)))
            {
                ReleaseAll();

                return ValidationState.KEEP_GOING;
            }

            ValidationState validate = ValidateRemove();
            if (validate.Equals(ValidationState.FAILURE))
            {
                ReleaseAll();
                return validate;
            }

            if (validate.Equals(ValidationState.KEEP_GOING))
            {
                ReleaseAll();
                return validate;
            }

            if (resultNode.GetValue().Equals(default(T)))
            {
                ReleaseAll();
                return ValidationState.FAILURE;
            }
            
            List<Node<T>> childEdges = resultNode.GetOutGoingEdgesList();
            if (childEdges.Size() == 1)
            {
                Node<T> child = childEdges.GetIndex(0);
                string concatenatedEdges = RadixTreeUtils<T>.Concatenate(resultNode.GetIncomingEdges(), child.GetIncomingEdges()).ToString();
                Node<T> mergedNode = new Node<T>(child.GetValue(), concatenatedEdges, child.GetOutGoingEdgesList(), false);
                parent.UpdateOutEdgeByChildNode(mergedNode);
            }
            else if (childEdges.Size() > 1)
            {                
                Node<T> nodeCopy = new Node<T>(default(T), resultNode.GetIncomingEdges(), resultNode.GetOutGoingEdgesList(), false);                
                parent.UpdateOutEdgeByChildNode(nodeCopy);
            }            
            else
            {
                /* In case of a node without children */                
                List<Node<T>> currentEdgesFromParent = parent.GetOutGoingEdgesList();                
                List<Node<T>> newEdgesOfParent = new List<Node<T>>();
                int numParentEdges = currentEdgesFromParent.Size();
                for (int i = 0; i < numParentEdges; i++)
                {
                    Node<T> node = currentEdgesFromParent.GetIndex(i);
                    if (node != resultNode)
                    {
                        newEdgesOfParent.Add(node);
                    }
                }

                bool isParentRoot = false;
                if(parent == currentTree.root)
                {
                    isParentRoot = true;
                }
                
                Node<T> newParent;
                if (newEdgesOfParent.Size() == 1 && parent.GetValue().Equals(default(T)) && !isParentRoot)
                {
                    Node<T> parentsRemainingChild = newEdgesOfParent.GetIndex(0);
                    string concatenatedEdges = RadixTreeUtils<T>.Concatenate(parent.GetIncomingEdges(), parentsRemainingChild.GetIncomingEdges()).ToString();
                    newParent = new Node<T>(parentsRemainingChild.GetValue(), concatenatedEdges, parentsRemainingChild.GetOutGoingEdgesList(), isParentRoot);
                }
                else
                {                    
                    newParent = new Node<T>(parent.GetValue(), parent.GetIncomingEdges(), newEdgesOfParent, isParentRoot);
                }
                
                if (isParentRoot)
                {               
                    lock(currentTree)
                    {
                        currentTree.root = newParent;
                    }
                }
                else
                {
                    grandParent.UpdateOutEdgeByChildNode(newParent);
                }
            }

            ReleaseAll();

            return ValidationState.SUCCESS;
        }

        private ValidationState ValidateRemove()
        {
            SearchEntities<T> validationResult = currentTree.searchEntityFactory.CreateSearchEntity(key);
            if (validationResult.GetType()!=this.GetType())
            {
                return ValidationState.FAILURE;
            }
            if (validationResult.parent != parent || validationResult.grandParent != grandParent || validationResult.resultNode != resultNode)
            {
                return ValidationState.KEEP_GOING;
            }
            return ValidationState.SUCCESS;
        }
    }
}
