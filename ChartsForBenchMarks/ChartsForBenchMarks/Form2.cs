﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ChartsForBenchMarks
{
    public partial class Form2 : Form
    {
        public Form2(string path)
        {
            InitializeComponent();
            Series s1 = new Series();
            chart1.ChartAreas[0].AxisX.Title = "Number of Nodes";
            chart1.ChartAreas[0].AxisY.Title = "Time [ms]";
            s1.Name = "Non Concurrent";

            DirectoryInfo dir = new DirectoryInfo(path);

            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                using (var reader = new StreamReader(file.FullName))
                {
                    var responseBody = reader.ReadToEnd();
                    MeasurementsJson[] measures = js.Deserialize<MeasurementsJson[]>(responseBody);
                    if ("00:00:00" == measures[0].NonConcurrentResult) continue;
                    double milliCG = TimeSpan.Parse(measures[0].NonConcurrentResult).TotalMilliseconds;
                    double keys = measures[0].NumberOfKeys;
                    s1.Points.AddXY(keys, milliCG);
                }
            }
            s1.Sort(PointSortOrder.Ascending);
            s1.ChartType = SeriesChartType.Line;
            chart1.Series.Add(s1);
            Refresh();
        }

        public class MeasurementsJson
        {
            public int NumberOfKeys { get; set; }
            public int PercantageRemoval { get; set; }
            public int PercantageInserts { get; set; }
            public int NumberOfInserts { get; set; }
            public int NumberOfRemovals { get; set; }
            public int TreeDepth { get; set; }

            public int NumberOfThreads { get; set; }

            public int IsBulk { get; set; }
            public int NonConcurrentMeasured { get; set; }

            public string NonConcurrentResult { get; set; }
            public string CourseGrainedLockResult { get; set; }
            public string FineGrainedLockResult
            {
                get; set;
            }
        }
    }
}
