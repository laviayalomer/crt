﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace RadixTree_CoarseGrained
{
    public class CRT<T>
    {
        private static readonly Object obj = new Object();

        public volatile Node<T> root;
        public Object locker = new Object();
        public SearchEntities_Factory<T> searchEntityFactory;

        public CRT()
        {
            root = new Node<T>(default(T), "", new List<Node<T>>(), true);
            searchEntityFactory = new SearchEntities_Factory<T>(this);
        }

        public IEnumerable<T> SearchChildrenValues(string startKey, Node<T> startNode)
        {
            return new Enumerators<T>.BaseValuesEnumerable(startNode, startKey);
        }

        public T SearchValueOfSpecificKey(string key)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
            return result.GetValueForKeyFound();
        }

        public IEnumerable<string> SearchKeysAccordingToPrefix(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetKeysWithGivenPrefix();
        }

        public IEnumerable<T> SearchValuesOfKeysAccordingToPrefix(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetValuesOfKeysWithGivenPrefix();
        }

        public IEnumerable<KeyValuePair<string, T>> SearchKeyValuePairsAccordingToPrefix(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetKeyValuePairsOfKeysWithGivenPrefix();
        }        

        public IEnumerable<string> SearchSimilarKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetKeysWithLongestGivenPrefix();
        }

        public IEnumerable<T> SearchValuesOfSimilarKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetValuesOfKeysWithLongestGivenPrefix();
        }

        public IEnumerable<KeyValuePair<string, T>> SearchKeyValuePairsOfSimilarKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetKeyValuePairsOfKeysWithLongestGivenPrefix();
        }

        public int Size()
        {
            LinkedList<Node<T>> stack = new LinkedList<Node<T>>();
            stack.AddFirst(root);
            int count = 0;
            while (true)
            {
                if (stack.Count == 0)
                {
                    return count;
                }
                Node<T> current = stack.First();
                stack.RemoveFirst();
                stack.AddAll(current.GetOutGoingEdgesList());
                if (!current.GetValue().Equals(default(T)))
                {
                    count++;
                }
            }
        }        

        public IEnumerable<string> SearchChildernKeys(string startKey, Node<T> startNode)
        {
            return (IEnumerable<string>)(new Enumerators<T>.BaseKeysEnumerable<string>(startNode, startKey));
        }

        public IEnumerable<KeyValuePair<string, T>> SearchChildernKeyValuePairs(string startKey, Node<T> startNode)
        {
            return (IEnumerable<KeyValuePair<string, T>>)new Enumerators<T>.BaseKeyValuePairsEnumerable(startNode, startKey);
        }

        public IEnumerable<NodeKeyPair<T>> TraverseChildren(string startKey, Node<T> startNode)
        {
            return new Enumerators<T>.BaseTreeTraverseEnumerable(startNode, startKey);
        }

        public T Insert(string key, T value, bool overwrite = true)
        {
            if (key == null)
            {
                throw new InnerExceptions.KeyIsNullException();
            }
            if (key.Length == 0)
            {
                throw new InnerExceptions.KeyInsertedOfZeroLengthException();
            }
            if (value.Equals(default(T)))
            {
                throw new InnerExceptions.ValueInsertedhDefaultException();
            }
            SearchEntities<T>.ValidationState insertResult;
            T res = default(T);
            lock (obj)
            {        
                    SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                    insertResult = result.InsertSearchEntities(overwrite, value, ref res);
            }

            if (insertResult.Equals(SearchEntities<T>.ValidationState.SUCCESS))
            {
                return res;
            }
            return default(T);
        }                

        public bool Remove(string key)
        {
            if (key == null)
            {
                throw new InnerExceptions.KeyIsNullException();
            }
            lock (obj)
            {
                SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                SearchEntities<T>.ValidationState removeResult = result.RemoveSearchEntities();
                if (removeResult.Equals(SearchEntities<T>.ValidationState.FAILURE))
                {
                    return false;
                }
                else if (removeResult.Equals(SearchEntities<T>.ValidationState.SUCCESS))
                {
                    return true;
                }
            }
            return false;
        }
    }
}