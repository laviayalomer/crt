﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RadixTree
{
    public class SearchEntities_PartOfKeyFound<T> : SearchEntities<T>
    {
        public SearchEntities_PartOfKeyFound(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {
        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<string>();
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            if (totalNumOfEqualChars == 0)
            {
                return new Enumerators<T>.EmptyBaseEnumerable<string>();
            }            
            string keyOfNodeFound = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars);
            return currentTree.SearchChildernKeys(keyOfNodeFound, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
        }
        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {
            if (totalNumOfEqualChars == 0)
            {
                return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
            }
            
            string keyOfNodeFound = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars);
            return currentTree.SearchChildernKeyValuePairs(keyOfNodeFound, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return default(T);
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<T>();
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {
            if (totalNumOfEqualChars == 0)
            {
                return new Enumerators<T>.EmptyBaseEnumerable<T>();
            }
            string keyOfNodeFound = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars);
            return currentTree.SearchChildrenValues(keyOfNodeFound, resultNode);
        }

        public override ValidationState InsertSearchEntities(bool overwrite, T value, ref T res)
        {
            Monitor.Enter(currentTree.locker);

            if (!(Monitor.TryEnter(resultNode) && (parent == null ? true : Monitor.TryEnter(parent)) && (grandParent == null ? true : Monitor.TryEnter(grandParent)) && AcquireLockOnChildren()))
            {
                ReleaseAll();
                Monitor.Exit(currentTree.locker);

                return ValidationState.KEEP_GOING;
            }
            Monitor.Exit(currentTree.locker);

            ValidationState validate = ValidateInsert();
            if (!validate.Equals(ValidationState.SUCCESS))
            {
                ReleaseAll();
                return validate;
            }         
            string keySuffix = key.Substring(totalNumOfEqualChars);
            Node<T> newChild = new Node<T>(value, keySuffix, new List<Node<T>>(), false);

            List<Node<T>> edges = new List<Node<T>>(resultNode.GetOutGoingEdgesList().Size() + 1);
            edges.AddAll(resultNode.GetOutGoingEdgesList());
            edges.Add(newChild);
            Node<T> nodeCopy = new Node<T>(resultNode.GetValue(), resultNode.GetIncomingEdges(), edges, resultNode == currentTree.root);

            if (resultNode == currentTree.root)
            {
                lock (currentTree)
                {
                currentTree.root = nodeCopy;
				}
            }
            else
            {
                parent.UpdateOutEdgeByChildNode(nodeCopy);
            }

            ReleaseAll();
            return ValidationState.SUCCESS;
        }

        public override ValidationState RemoveSearchEntities()
        {
            return ValidationState.FAILURE;
        }
    }
}
