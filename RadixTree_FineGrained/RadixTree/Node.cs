﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RadixTree
{
    public class Node<T>
    {
        private string IncomingEdge;
        private ConcurrentArray<Node<T>> OutGoingEdges;
        private List<Node<T>> OutGoingEdgesList;
        private T value;

        public Node(T value, string IncomingEdge, List<Node<T>> OutGoingEdges, bool isRoot = false)
        {           
            CheckNodeArguments(IncomingEdge, OutGoingEdges, isRoot);
            CheckForDuplicateEdges(OutGoingEdges); 
            if (OutGoingEdges.Size() == 0)
            {
                LeafNode_Config(value, IncomingEdge);
            }
            else
            {
                InnerNode_Config(value, IncomingEdge, OutGoingEdges);
            }
        }

        /// <summary>
        /// Checks the validity of the arguments
        /// </summary>
        /// <param name="IncomingEdges"></param>
        /// <param name="childNodes"></param>
        /// <param name="isRoot"></param>
        private void CheckNodeArguments(string IncomingEdge, List<Node<T>> childNodes, bool isRoot)
        {
            if (IncomingEdge == null)
            {
                throw new InnerExceptions.InvalidEdgesException();
            }
            if (!isRoot && IncomingEdge.Length == 0)
            {
                throw new InnerExceptions.InvalidEdgesException();
            }
            if (childNodes == null)
            {
                throw new ArgumentNullException("The childNodes argument was null");
            }
        }

        /// <summary>
        /// Config the node as an inner node & initialize the OutGoingEdges & OutGoingEdgesList members
        /// </summary>
        /// <param name="value"></param>
        /// <param name="IncomingEdge"></param>
        /// <param name="OutGoingEdges"></param>
        private void InnerNode_Config(T value, string IncomingEdge, List<Node<T>> OutGoingEdges)
        {
            Node<T>[] OutGoingEdgesArray = OutGoingEdges.ListToArray();
            Array.Sort(OutGoingEdgesArray, new NodeComparator());
            this.OutGoingEdges = new ConcurrentArray<Node<T>>(OutGoingEdgesArray);
            this.IncomingEdge = IncomingEdge;
            this.value = value;
            OutGoingEdgesList = new ConcurrentArrayAsList<Node<T>>(this.OutGoingEdges);
        }

        /// <summary>
        /// Config the node as a leaf node 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="IncomingEdges"></param>
        private void LeafNode_Config(T value, string IncomingEdges)
        {
            OutGoingEdgesList = new List<Node<T>>();
            this.IncomingEdge = IncomingEdges;
            this.value = value;
        }

        //====>> Getters <<====//

        /// <summary>
        /// Gets the In Edges
        /// </summary>
        /// <returns></returns>
        public string GetIncomingEdges()
        {
            return IncomingEdge;
        }

        /// <summary>
        /// Gets the first char in the In edges
        /// </summary>
        /// <returns></returns>
        public char GetFirstIncomingEdges()
        {
            return IncomingEdge[0];
        }

        public T GetValue()
        {
            return value;
        }

        /// <summary>
        /// Gets the OutGoingEdges as list of nodes
        /// </summary>
        /// <returns></returns>
        public List<Node<T>> GetOutGoingEdgesList()
        {
            return OutGoingEdgesList;
        }

        /// <summary>
        /// Gets the node of the OutGoingEdges by a given char
        /// </summary>
        /// <param name="edgeFirstCharacter"></param>
        /// <returns></returns>
        public Node<T> GetOutEdgeByFirstChar(char edgeFirstCharacter)
        {
            int index;

            //Indicates inner Node
            if (OutGoingEdgesList.Size() != 0 && (index = BinarySearchForEdge(OutGoingEdges, edgeFirstCharacter)) >= 0)    
            {
                return OutGoingEdges.Get(index);
            }

            return null;
        }

        /// <summary>
        /// Updates the OutGoingEdges of the given node
        /// </summary>
        /// <param name="childNode"></param>
        public void UpdateOutEdgeByChildNode(Node<T> node)
        {
            int index;

            //Indicates inner Node
            if (OutGoingEdgesList.Size() != 0 && (index = BinarySearchForEdge(OutGoingEdges, node.GetFirstIncomingEdges())) >= 0)
            {
                OutGoingEdges.ConcurrentSet(index, node);
                return;
            }

            throw new Exception("Cannot update the reference to the following child node for the edge starting with '" + node.GetFirstIncomingEdges() + "', no such edge already exists: " + node);
        }
        
        override public string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Node =[Value =");

            if (!value.Equals(default(T))) { stringBuilder.Append(value); }
            else { stringBuilder.Append("null"); }

            stringBuilder.Append(", IncomingEdges = "+ (IncomingEdge.Length==0?"[]": IncomingEdge));

            if(OutGoingEdgesList.Size() == 0) { stringBuilder.Append(", OutGoingEdges=[]"); }
            else { stringBuilder.Append(", OutGoingEdges=" + OutGoingEdgesList.ToString()); }

            return stringBuilder.ToString();
        }

        //====>> Helper Methods <<====//

        /// <summary>
        /// Search for the index of the given 'FirstCharOfEdge' by a binary search
        /// </summary>
        /// <param name="OutGoingEdges"></param>
        /// <param name="FirstCharOfEdge"></param>
        /// <returns></returns>
        private int BinarySearchForEdge(ConcurrentArray<Node<T>> OutGoingEdges, char FirstCharOfEdge)
        {
            int low = 0;
            int high = OutGoingEdges.size - 1;

            while (low <= high)
            {
                int middle = (low + high) / 2;
                int CompareRes = OutGoingEdges.Get(middle).GetFirstIncomingEdges().CompareTo(FirstCharOfEdge);

                if (CompareRes == 0)
                {
                    return middle;
                }
                else if (CompareRes < 0)
                {
                    low = middle + 1;
                }
                else
                {
                    high = middle - 1;
                }
            }

            return -1;
        }

        /// <summary>
        /// Verify that the node list has no duplications on its edges
        /// </summary>
        /// <param name="nodeList"></param>
        private void CheckForDuplicateEdges(List<Node<T>> nodeList)
        {
            ICollection<char> distinctChars = new HashSet<char>();
            for (int i = 0; i < nodeList.Size(); i++)
            {
                distinctChars.Add(nodeList.GetIndex(i).GetFirstIncomingEdges());
            }
            if (nodeList.Size() != distinctChars.Count)
            {
                throw new InnerExceptions.DuplicateEdgeDetected("Duplicate edge detected in list of nodes supplied: " + nodeList);
            }
        }

        //====>> Helper Class for comparsion of Node<T> <<====//

        private class NodeComparator : IComparer<Node<T>>
        {
            int IComparer<Node<T>>.Compare(Node<T> x, Node<T> y)
            {
                return x.GetFirstIncomingEdges().CompareTo(y.GetFirstIncomingEdges());
            }
        }
    }
}
