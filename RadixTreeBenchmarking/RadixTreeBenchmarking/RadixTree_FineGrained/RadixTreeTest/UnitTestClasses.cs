﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.IO;
using System.Collections;

namespace RadixTree_FineGrained
{
    [TestClass]
    public class UnitTest
    {
        int i;
        string nodeSign = ((char)0x0298).ToString();

        [TestMethod]
        public void Test_01_BuildNaiveTree()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            String staticTreeResult =
                    nodeSign + "\n" +
                    "└── " + nodeSign + " [HELLO, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_02_SetValue()
        {
            CRT<int> tree = new CRT<int>();

            int result;
            result = tree.Insert("A", 1);
            Assert.IsTrue(int.Equals(result, 0));
            result = tree.Insert("A", 2);
            Assert.IsTrue(int.Equals(result, 1));

            Assert.IsTrue(Equals(2, tree.SearchValueOfSpecificKey("A")));
        }

        [TestMethod]
        public void Test_03_CheckNodeSorting()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("B", 1);
            tree.Insert("A", 2);
            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "├── " + nodeSign + " [A, 2]\n" +
                    "└── " + nodeSign + " [B, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_04_AppendSubstringNode()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 1]\n" +
                    "    └── " + nodeSign + " [LLO, 2]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_05_CheckNodeSplitting()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO", 1);
            tree.Insert("HE", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 2]\n" +
                    "    └── " + nodeSign + " [LLO, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_06_AppendNodeCommonPrefix()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " HE\n" +
                    "    ├── " + nodeSign + " [LLO, 1]\n" +
                    "    └── " + nodeSign + " [RE, 2]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_07_AppendNodeCommonPrefixComplex()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " E\n" +
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_08_CheckOverrideValue()
        {
            CRT<int> tree = new CRT<int>();
            int result;

            result = tree.Insert("A", 1, false);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.Insert("A", 2, false);
            Assert.IsTrue(int.Equals(result, 1));

            Assert.IsTrue(Equals(1, tree.SearchValueOfSpecificKey("A")));
        }

        [TestMethod]
        public void Test_09_CheckInsertAndSplitNode()
        {
            CRT<int> tree = new CRT<int>();
            int result;

            result = tree.Insert("HELLO", 1, false);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.Insert("HERE", 1, false);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.Insert("HE", 2, false);
            Assert.IsTrue(int.Equals(result, 0));
        }

        [TestMethod]
        public void Test_10_keyValidation()
        {
            CRT<int> tree = new CRT<int>();
            try
            {
                tree.Insert(null, 1);
            }
            catch (Exception)
            {
                Console.WriteLine("Got the exception staticTreeResult");
            }
        }

        [TestMethod]
        public void Test_11_valueValidation()
        {
            CRT<int> tree = new CRT<int>();
            try
            {
                tree.Insert("A", 0);
            }
            catch (Exception)
            {
                Console.WriteLine("Got the exception staticTreeResult");
            }
        }

        [TestMethod]
        public void Test_12_BuildComplexString()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO_MY_FRIEND", 6);
            tree.Insert("HELLO_MY", 5);
            tree.Insert("HELLO_THERE", 4);
            tree.Insert("HELLO_", 3);
            tree.Insert("HE", 2);
            tree.Insert("H", 1);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [H, 1]\n" +
                    "    └── " + nodeSign + " [E, 2]\n" +
                    "        └── " + nodeSign + " [LLO_, 3]\n" +
                    "            ├── " + nodeSign + " [MY, 5]\n" +
                    "            │   └── " + nodeSign + " [_FRIEND, 6]\n" +
                    "            └── " + nodeSign + " [THERE, 4]\n";

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_13_CheckSize()
        {
            CRT<int> tree = new CRT<int>();

            Assert.IsTrue(int.Equals(0, tree.Size()));

            tree.Insert("HELLO", 1);
            Assert.IsTrue(int.Equals(1, tree.Size()));

            tree.Insert("HERE", 2);
            Assert.IsTrue(int.Equals(2, tree.Size()));

            tree.Insert("HOUSE", 3);
            Assert.IsTrue(int.Equals(3, tree.Size()));

            tree.Remove("B");
            Assert.IsTrue(int.Equals(3, tree.Size()));

            tree.Remove("HOUSE");
            Assert.IsTrue(int.Equals(2, tree.Size()));

            tree.Remove("HERE");
            Assert.IsTrue(int.Equals(1, tree.Size()));

            tree.Remove("HELLO");
            Assert.IsTrue(int.Equals(0, tree.Size()));
        }

        [TestMethod]
        public void Test_14_CheckGetValueForKey()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);

            Assert.IsTrue(int.Equals(1, tree.SearchValueOfSpecificKey("HELLO")));
            Assert.IsTrue(int.Equals(2, tree.SearchValueOfSpecificKey("HERE")));
            Assert.IsTrue(int.Equals(3, tree.SearchValueOfSpecificKey("HOUSE")));
            Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("H")));
            Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("HE")));
            Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("E")));
            Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("")));
        }

        [TestMethod]
        public void Test_15_CheckRemove_MoreThanOneChildEdge()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("H", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HAM", 3);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [H, 1]\n" +
                    "    ├── " + nodeSign + " [AM, 3]\n" +
                    "    └── " + nodeSign + " [ELLO, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("H");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [AM, 3]\n" +
                    "    └── " + nodeSign + " [ELLO, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_16_CheckRemove_ExactlyOneChildEdge()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("H", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HELLO_WORLD", 3);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [H, 1]\n" +
                    "    └── " + nodeSign + " [ELLO, 2]\n" +
                    "        └── " + nodeSign + " [_WORLD, 3]\n";
            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("H");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HELLO, 2]\n" +
                    "    └── " + nodeSign + " [_WORLD, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_17_CheckRemove_ZeroChildEdges_DirectChildOfRoot()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("A", 1);
            tree.Insert("B", 2);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "├── " + nodeSign + " [A, 1]\n" +
                    "└── " + nodeSign + " [B, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("A");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [B, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_18_CheckRemove_LastRemainingKey()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("A", 5);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [A, 5]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("A");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_19_CheckRemove_ZeroChildEdges_OneStepFromRoot()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 1]\n" +
                    "    └── " + nodeSign + " [LLO, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("HELLO");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 1]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_20_CheckRemove_ZeroChildEdges_SeveralStepsFromRoot()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HELLO_WORLD", 3);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 1]\n" +
                    "    └── " + nodeSign + " [LLO, 2]\n" +
                    "        └── " + nodeSign + " [_WORLD, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("HELLO_WORLD");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 1]\n" +
                    "    └── " + nodeSign + " [LLO, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_21_CheckRemove_DoNotRemoveSplitNode()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Insert("HELLO", 1);
            radixTree.Insert("HERE", 2);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " HE\n" +
                    "    ├── " + nodeSign + " [LLO, 1]\n" +
                    "    └── " + nodeSign + " [RE, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HE");
            Assert.IsFalse(Removed);

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_22_CheckRemove_MergeSplitNode()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Insert("HELLO", 1);
            radixTree.Insert("HERE", 2);
            radixTree.Insert("HOUSE", 3);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " E\n" +
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HELLO");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [ERE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_23_CheckRemove_DoNotMergeSplitNodeWithValue()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Insert("HELLO", 1);
            radixTree.Insert("HERE", 2);
            radixTree.Insert("HOUSE", 3);
            radixTree.Insert("HE", 4);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [E, 4]\n" +
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HELLO");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [E, 4]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        public void TEST_24_CheckRemove_NoSuchKey()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("A", 1);
            tree.Insert("B", 2);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "├── " + nodeSign + " [A, 1]\n" +
                    "└── " + nodeSign + " [B, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("HELLO");
            Assert.IsFalse(Removed);

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_25_CheckGetKeysForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);
            tree.Insert("HER", 4);
            tree.Insert("A", 5);

            Assert.IsTrue(String.Equals("[A, HELLO, HER, HERE, HOUSE]", ToString(tree.SearchKeysAccordingToPrefix(""))));
            Assert.IsTrue(String.Equals("[A]", ToString(tree.SearchKeysAccordingToPrefix("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchKeysAccordingToPrefix("AB"))));
            Assert.IsTrue(String.Equals("[HELLO, HER, HERE, HOUSE]", ToString(tree.SearchKeysAccordingToPrefix("H"))));
            Assert.IsTrue(String.Equals("[HELLO, HER, HERE]", ToString(tree.SearchKeysAccordingToPrefix("HE"))));
            Assert.IsTrue(String.Equals("[HER, HERE]", ToString(tree.SearchKeysAccordingToPrefix("HER"))));
            Assert.IsTrue(String.Equals("[HOUSE]", ToString(tree.SearchKeysAccordingToPrefix("HO"))));
        }

        [TestMethod]
        public void TEST_26_CheckSearchSimilarKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HAM", 3);

            Assert.IsTrue(String.Equals("[HAM, HE, HELLO]", ToString(tree.SearchSimilarKeys("HI"))));
            Assert.IsTrue(String.Equals("[HE, HELLO]", ToString(tree.SearchSimilarKeys("HE"))));
            Assert.IsTrue(String.Equals("[HELLO]", ToString(tree.SearchSimilarKeys("HELL"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchSimilarKeys("B"))));
            Assert.IsTrue(String.Equals("[HELLO]", ToString(tree.SearchSimilarKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_27_CheckSearchValuesOfSimilarKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HAM", 3);

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);

            Assert.IsTrue(String.Equals("[3, 1, 2]", ToString(tree.SearchValuesOfSimilarKeys("HI"))));
            Assert.IsTrue(String.Equals("[1, 2]", ToString(tree.SearchValuesOfSimilarKeys("HE"))));
            Assert.IsTrue(String.Equals("[3]", ToString(tree.SearchValuesOfSimilarKeys("HA"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchValuesOfSimilarKeys("B"))));
            Assert.IsTrue(String.Equals("[2]", ToString(tree.SearchValuesOfSimilarKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_28_CheckSearchKeyValuePairsOfSimilarKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HAM", 3);

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);

            Assert.IsTrue(String.Equals("[[HAM, 3], [HE, 1], [HELLO, 2]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HI"))));
            Assert.IsTrue(String.Equals("[[HE, 1], [HELLO, 2]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HE"))));
            Assert.IsTrue(String.Equals("[[HAM, 3]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HA"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("B"))));
            Assert.IsTrue(String.Equals("[[HELLO, 2]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_29_CheckKeyValuePair_Accessor()
        {
            KeyValuePair<string, int> pair = new KeyValuePair<string, int>("A", 3);
            Assert.IsTrue(String.Equals(pair.Key, "A"));
            Assert.IsTrue(int.Equals(pair.Value, 3));
            String p = pair.ToString();
            Assert.IsTrue(String.Equals("[A, 3]", p));
        }

        [TestMethod]

        public void TEST_31_CheckGetValuesForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);
            tree.Insert("HER", 4);
            tree.Insert("A", 5);

            Assert.IsTrue(String.Equals("[5, 1, 4, 2, 3]", ToString(tree.SearchValuesOfKeysAccordingToPrefix(""))));
            Assert.IsTrue(String.Equals("[5]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("AB"))));
            Assert.IsTrue(String.Equals("[1, 4, 2, 3]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("H"))));
            Assert.IsTrue(String.Equals("[1, 4, 2]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("HE"))));
            Assert.IsTrue(String.Equals("[4, 2]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("HER"))));
            Assert.IsTrue(String.Equals("[3]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("HO"))));
        }

        [TestMethod]
        public void TEST_32_CheckGetKeyValuePairsForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);
            tree.Insert("HER", 4);
            tree.Insert("A", 5);

            Assert.IsTrue(String.Equals("[[A, 5], [HELLO, 1], [HER, 4], [HERE, 2], [HOUSE, 3]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix(""))));
            Assert.IsTrue(String.Equals("[[A, 5]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("AB"))));
            Assert.IsTrue(String.Equals("[[HELLO, 1], [HER, 4], [HERE, 2], [HOUSE, 3]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("H"))));
            Assert.IsTrue(String.Equals("[[HELLO, 1], [HER, 4], [HERE, 2]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("HE"))));
            Assert.IsTrue(String.Equals("[[HER, 4], [HERE, 2]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("HER"))));
            Assert.IsTrue(String.Equals("[[HOUSE, 3]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("HO"))));
        }

        [TestMethod]
        public void TEST_33_CheckSizeAdvanced()
        {
            CRT<int> tree = new CRT<int>();
            const int numOfKeys = 1000;

            string[] keysArray = new string[numOfKeys];
            keysArray[0] = "NOT_VALID";

            for (i = 1; i < numOfKeys; i++)
            {
                keysArray[i] = MultiThreadUnitTest.RandomString(i);
                tree.Insert(keysArray[i], i);
            }
            for (i = 1; i < numOfKeys; i++)
            {
                tree.Remove(keysArray[i]);
            }


            Assert.IsTrue(int.Equals(0, tree.Size()));
        }        

        private object ToString(IEnumerable<KeyValuePair<string, int>> enumerable)
        {
            string sb = "[";
            IEnumerator<KeyValuePair<string, int>> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }

        private object ToString(IEnumerable<int> enumerable)
        {
            string sb = "[";
            IEnumerator<int> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }

        private object ToString(IEnumerable<string> enumerable)
        {
            string sb = "[";
            IEnumerator<string> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }
    }


    [TestClass]
    public class MultiThreadUnitTest
    {
        const int advancedNumOfThreads = 88;
        const int KeysLength = 20;
        const int numOfWords = 100000;
        string nodeSign = ((char)0x0298).ToString();
        static CRT<int> tree = new CRT<int>();
        static CRT<int> testTree = new CRT<int>();
        static int globalIndex = 0;
        static List<KeyValuePair<string, int>>[] arrList;

        [TestMethod]
        public void TEST_34_MultiThreadCheckSizeBasic()
        {
            int i, numOfThreads = 200;
            String dynamicTreeResult;
            Thread[] Threads = new Thread[numOfThreads];


            for (i = 0; i < numOfThreads; i++)
            {
                Threads[i] = new Thread(new ThreadStart(StartInsertAndRemove_Basic));
                Threads[i].Name = "Thread_Number_" + i.ToString();
            }

            for (i = 0; i < numOfThreads; i++)
            {
                Threads[i].Start();
            }

            for (i = 0; i < numOfThreads; i++)
            {
                Threads[i].Join();
            }

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Debug.WriteLine(dynamicTreeResult);
            Assert.IsTrue(int.Equals(0, tree.Size()));


            for (i = 0; i < numOfThreads; i++)
            {
                Threads[i] = new Thread(new ThreadStart(StartInsertAndRemove_Intermediate));
                Threads[i].Name = "Thread_Number_" + i.ToString();
            }

            for (i = 0; i < numOfThreads; i++)
            {
                Threads[i].Start();
            }

            for (i = 0; i < numOfThreads; i++)
            {
                Threads[i].Join();
            }

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Debug.WriteLine(dynamicTreeResult);
            Assert.IsTrue(int.Equals(0, tree.Size()));
        }

        [TestMethod]
        public void TEST_35_MultiThreadCheckSizeAdvanced()
        {
            int i;
            Thread[] Threads_Advanced = new Thread[advancedNumOfThreads];

            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i] = new Thread(new ThreadStart(StartInsertAndRemove_Advanced));
                Threads_Advanced[i].Name = "Thread_Number_" + i.ToString();
            }

            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Debug.WriteLine(Threads_Advanced[i].Name);
                Threads_Advanced[i].Start();
            }

            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i].Join();
            }

            Assert.IsTrue(int.Equals(0, tree.Size()));
        }

        
        public void TEST_36_MultiThreadCreateTree()
        {
            globalIndex = 0;
            List<KeyValuePair<string, int>> allStringsToTree = GetRandomKeyValueList();
            foreach(KeyValuePair<string, int> pair in allStringsToTree)
            {
                testTree.Insert(pair.Key, pair.Value);
            }
            arrList = GetRandomKeyValueListThreads(allStringsToTree);

            Thread[] Threads_Advanced = new Thread[advancedNumOfThreads];
            int i;
            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i] = new Thread(new ThreadStart(StartInsert_Advanced));
                Threads_Advanced[i].Name = "Thread_Number_" + i.ToString();
            }

            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i].Start();
                globalIndex++;
            }
            globalIndex--;
            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i].Join();
            }
            String concurrentTree = MyStringBuilder<int>.BuildStringFromTree(tree);
            String staticTree = MyStringBuilder<int>.BuildStringFromTree(tree);

            Assert.IsTrue(String.Equals(staticTree, concurrentTree));
        }
        [TestMethod]
        public void TEST_37_MultiThreadRealTimeSearch()
        {
            int i;
            Thread[] Threads_Advanced = new Thread[advancedNumOfThreads];
            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i] = new Thread(new ThreadStart(StartInsertSearchAndRemove));
            }
            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i].Start();
            }
            for (i = 0; i < advancedNumOfThreads; i++)
            {
                Threads_Advanced[i].Join();
            }
            Assert.IsTrue(int.Equals(0, tree.Size()));
        }
        private List<KeyValuePair<string, int>>[] GetRandomKeyValueListThreads(List<KeyValuePair<string, int>> lst)
        {
            List<KeyValuePair<string, int>>[] allStringsToTree = new List<KeyValuePair<string, int>>[advancedNumOfThreads];
            int randLength;
            int m = 0;
            Random random = new Random();
            int[] pointers = new int[advancedNumOfThreads];
            while (m < advancedNumOfThreads)
            {
                randLength = random.Next(numOfWords);
                if (pointers.Contains(randLength)) continue;
                pointers[m] = randLength;
                m++;
            }
            Array.Sort(pointers);
            pointers[advancedNumOfThreads-1] = numOfWords;
            int count = 0;
            for (int i=0; i < advancedNumOfThreads; i++)
            {
                allStringsToTree[i] = lst.GetRange(count, pointers[i]-count);
                count = pointers[i];
            }

            return allStringsToTree;
        }

        private List<KeyValuePair<string, int>> GetRandomKeyValueList()
        {
            List<KeyValuePair<string, int>> allStringsToTree = new List<KeyValuePair<string, int>>();
            int randVal, randLength;
            string randString;
            Random random = new Random();
            for (int k = 0; k < numOfWords; k++)
            {
                randLength = random.Next(1, 100);
                randVal = random.Next(1, int.MaxValue);
                randString = RandomString(randLength);
                KeyValuePair<string, int> keyValue = new KeyValuePair<string, int>(randString, randVal);
                allStringsToTree.Add(keyValue);
            }

            return allStringsToTree;
        }

        private void StartInsertAndRemove_Basic()
        {
            tree.Insert("A", 1);
            tree.Insert("B", 2);
            tree.Insert("C", 3);
            tree.Insert("D", 4);
            tree.Insert("E", 5);
            tree.Insert("F", 6);
            tree.Insert("G", 7);
            tree.Insert("H", 8);

            tree.Remove("A");
            tree.Remove("B");
            tree.Remove("C");
            tree.Remove("D");
            tree.Remove("E");
            tree.Remove("F");
            tree.Remove("G");
            tree.Remove("H");
        }

        private void StartInsertAndRemove_Intermediate()
        {
            tree.Insert("HELLO_MY_FRIEND", 6);
            tree.Insert("HELLO_MY", 5);
            tree.Insert("HELLO_THERE", 4);
            tree.Insert("HELLO_", 3);
            tree.Insert("HE", 2);
            tree.Insert("H", 1);

            tree.Remove("H");
            tree.Remove("HELLO_THERE");
            tree.Remove("HE");
            tree.Remove("HELLO_");
            tree.Remove("HELLO_MY_FRIEND");
            tree.Remove("HELLO_MY");
        }

        private void StartInsertAndRemove_Advanced()
        {
            int i;
            string[] keysArray = new string[numOfWords];
            keysArray[0] = "NOT_VALID";

            for (i = 1; i < numOfWords; i++)
            {
                keysArray[i] = RandomString(Math.Min(i,KeysLength));
                tree.Insert(keysArray[i], i);
            }
                
            for (i = 1; i < numOfWords; i++)
            {
                tree.Remove(keysArray[i]);
            }            
        }
        private void StartInsertSearchAndRemove()
        {
            int i;
            string str;
            string[] keysArray = new string[numOfWords];
            keysArray[0] = "NOT_VALID";
            for (i = 1; i < numOfWords; i++)
            {
                keysArray[i] = RandomString(Math.Min(i+6, KeysLength));
                tree.Insert(keysArray[i], i);
            }
            //String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            for (i = 1; i < numOfWords; i++)
            {
                int l = random.Next(1, 6);
                string s = RandomString(l);
                IEnumerable<string> e = tree.SearchKeysAccordingToPrefix(s);
                IEnumerator enumerator = e.GetEnumerator();
                if (enumerator.MoveNext())
                {
                    str = (string)enumerator.Current;
                    Assert.IsTrue(str.Length > 6);
                }
            }
            for (i = 1; i < numOfWords; i++)
            {
                tree.Remove(keysArray[i]);
                //Debug.WriteLine(keysArray[i]);
                //dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
                //Debug.WriteLine(dynamicTreeResult);
            }            
        }

        public static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void StartInsert_Advanced()
        {
            for (int i=0; i < arrList[globalIndex].Count; i++)
            {
                tree.Insert(arrList[globalIndex][i].Key, arrList[globalIndex][i].Value);
            }
        }

    }
}
