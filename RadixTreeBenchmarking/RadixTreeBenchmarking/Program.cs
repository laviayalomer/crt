﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RadixTreeBenchmarking
{
    class Program
    {
        private class CompareKeyValuesThreadsByKeys : IComparer
        {
            public CompareKeyValuesThreadsByKeys()
            { }
            int IComparer.Compare(object a, object b)
            {
                List<KeyValuePair<string, int>> lkv1 = (List<KeyValuePair<string, int>>)a;
                List<KeyValuePair<string, int>> lkv2 = (List<KeyValuePair<string, int>>)b;
                if (lkv1.Count > lkv2.Count)
                    return 1;
                if (lkv1.Count < lkv2.Count)
                    return -1;
                else
                    return 0;
            }
        }

        private static RadixTree_FineGrained.CRT<int> fgTree;
        private static RadixTree_CoarseGrained.CRT<int> cgTree;
        private static RadixTree_CoarseGrained.CRT<int> nonCCTree;

        private const string nonConcurrentBenchmarkRequest = "Do you want to enable non-concurrent benchmarks? press 1 if you do, 0 otherwise";
        private const string concurrentBulkRequest = "Do you want to enable fine-grained bulk operations? press 1 if you do, 0 otherwise";
        private const string KeepPerformingTestsRequest = "Do you want to run the test again? press 1 if you do, 0 otherwise";


        private const string numberOfKeysRequest = "Please enter the total number of keys in the tree";
        private const string maxTreeDepthRequest = "Please enter the maximal tree depth you want";
        private const string percentageOfInsertsRequest = "Please enter % of inserts in current benchmarks";
        private const string percentageOfRemovalsRequest = "Please enter % of Removals in current benchmarks";

        private static List<KeyValuePair<string, int>>[] keyValuesByThreadsCG;
        private static List<KeyValuePair<string, int>>[] keyValuesByThreadsFG;
        private static List<KeyValuePair<string, int>>[] lsts;
        private static RadixTree_FineGrained.CLL<int>[] concurrentLL;
        private static RadixTree_FineGrained.CLL<int>[] concurrentLLI;

        private static double removalFactor;

        private static TimeSpan nonCCtime;
        private static TimeSpan[] CGTime = new TimeSpan[15];
        private static TimeSpan[] FGTime = new TimeSpan[15];

        private static int[] numberOfThreadsInCurrentMeasurement = new int[] { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65 };
        private static int numberOfKeys;
        private static int maxTreeDepth;
        private static int percentageOfInserts;
        private static int percentageOfRemovals;
        private static int percentageOfSearches;
        private static int nonConcurrentBenchmark;
        private static int concurrentBulk;

        private static int numOfInserts;
        private static int numOfRemovals;
        private static int numOfSearches;
        private static int KeepPerformingTests = 1;
        private static int numOfInsertionThreads;

        private static string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

        static void Main(string[] args)
        {
            int res = ReadParametersFromUser();
            if (res == 1)
            {
                ConfigurationRun();
            }
            else
            {
                SpecificInputRun();
            }
        }

        private static void SpecificInputRun()
        {
            while (KeepPerformingTests == 1)
            {
                RunFlow();
                Console.WriteLine(KeepPerformingTestsRequest);
                KeepPerformingTests = int.Parse(Console.ReadLine());
            }
        }

        private static void RunFlow()
        {
            nonCCTree = new RadixTree_CoarseGrained.CRT<int>();

            var keyValuePairsnotR = GenerateRandomKeyValuePairs(maxTreeDepth, numberOfKeys);
            Random rnd = new Random();
            var keyValuePairs = keyValuePairsnotR.OrderBy(x => rnd.Next()).ToList();
            if (nonConcurrentBenchmark == 1)
            {
                StartNonConcurrentTreeMeasurement(keyValuePairs);
            }

            int k = 0;
            foreach (int numOfThreads in numberOfThreadsInCurrentMeasurement)
            {
                fgTree = new RadixTree_FineGrained.CRT<int>();
                cgTree = new RadixTree_CoarseGrained.CRT<int>();
                StartConcurrentMeasurementsWithNumberOfThreads(keyValuePairs, numOfThreads, k);
                k++;
            }

            PrintTimeSpans();
            CreateChartJson();
        }

        private static void CreateChartJson()
        {
            JavaScriptSerializer jss = new JavaScriptSerializer();
            MeasurementsJson[] measures = new MeasurementsJson[13];
            MeasurementsJson measure;
            for (int i = 0; i < 13; i++)
            {
                measure = new MeasurementsJson
                {
                    NumberOfThreads = numberOfThreadsInCurrentMeasurement[i],
                    IsBulk = concurrentBulk,
                    NonConcurrentMeasured = nonConcurrentBenchmark,
                    NonConcurrentResult = nonCCtime.ToString(),
                    CourseGrainedLockResult = CGTime[i].ToString(),
                    FineGrainedLockResult = FGTime[i].ToString(),
                    NumberOfInserts = numOfInserts,
                    NumberOfRemovals = numOfRemovals,
                    NumberOfKeys = numberOfKeys,
                    PercantageInserts=percentageOfInserts,
                    PercantageRemoval=percentageOfRemovals,
                    TreeDepth=maxTreeDepth
                };
                measures[i] = measure;
            }
            string jS = jss.Serialize(measures);
            string filename = path + @"/" + numberOfKeys + "_" + maxTreeDepth + "_" + percentageOfInserts + "_" + percentageOfRemovals + "_" + nonConcurrentBenchmark + "_" + concurrentBulk + ".json";
            using (StreamWriter sw = new StreamWriter(filename))
            {
                sw.WriteLine(jS);
            }
        }

        private static void ConfigurationRun()
        {
            string pathText = path;
            pathText += @"/ConfigFile.json";

            JObject jsonObj = JObject.Parse(File.ReadAllText(pathText));

            foreach (KeyValuePair<string, JToken> property in jsonObj)
            {
                JToken read = property.Value;
                JObject keyJson = JObject.Load(new JTokenReader(property.Value));

                numberOfKeys = (int)keyJson["numberOfKeys"];
                maxTreeDepth = (int)keyJson.SelectToken("maxTreeDepth");
                maxTreeDepth = TreeWordsGenerator.TreeWordsGenerator.CalculateNumOfChildren(numberOfKeys, maxTreeDepth);
                percentageOfInserts = (int)keyJson.SelectToken("percentageOfInserts");
                numOfInserts = (numberOfKeys / 100) * percentageOfInserts;
                percentageOfRemovals = (int)keyJson.SelectToken("percentageOfRemovals");
                numOfRemovals = (numberOfKeys / 100) * percentageOfRemovals;
                percentageOfSearches = 100 - (percentageOfInserts + percentageOfRemovals);
                nonConcurrentBenchmark = (int)keyJson.SelectToken("nonConcurrentBenchmark");
                concurrentBulk = (int)keyJson.SelectToken("concurrentBulk");

                RunFlow();
            }
        }

        private static List<string> RandomStringFilesGenerator(int treeDepth, int numOfKeys)
        {
            int randomDeletionIndex;
            int numberOfChildren = 0;
            string pathText = path + @"/RandomStrings/";
            string pathF = pathText + "AllWords" + numOfKeys + "," + treeDepth + ".txt";
            if (!File.Exists(pathF))
            {
                Console.WriteLine("Amount of keys not exist yet, creating suitable file, this might take a few sec");
                TreeWordsGenerator.TreeWordsGenerator.GenerateTextFileForDesiredTree(pathF, numOfKeys, treeDepth, ref numberOfChildren);
            }
            List<string> generatedRandomStrings = new List<string>(File.ReadAllLines(pathF));
            return generatedRandomStrings;

        }

        private static int ReadParametersFromUser()
        {
            Console.WriteLine("For configuration file benchmark test press 1\nFor specific input benchmark test press 0");
            int input = int.Parse(Console.ReadLine());
            if (input == 1)
            {
                return 1;
            }
            else
            {
                Console.WriteLine(numberOfKeysRequest);
                try
                {
                    numberOfKeys = int.Parse(Console.ReadLine());
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Overflow. taking max int");
                    numberOfKeys = int.MaxValue;
                }

                Console.WriteLine(maxTreeDepthRequest);
                maxTreeDepth = int.Parse(Console.ReadLine());
                maxTreeDepth = TreeWordsGenerator.TreeWordsGenerator.CalculateNumOfChildren(numberOfKeys, maxTreeDepth);
                Console.WriteLine(percentageOfInsertsRequest);
                percentageOfInserts = int.Parse(Console.ReadLine());
                numOfInserts = (numberOfKeys / 100) * percentageOfInserts;
                Console.WriteLine(percentageOfRemovalsRequest);
                percentageOfRemovals = int.Parse(Console.ReadLine());
                numOfRemovals = (numberOfKeys / 100) * percentageOfRemovals;
                percentageOfSearches = 100 - (percentageOfInserts + percentageOfRemovals);
                Console.WriteLine(nonConcurrentBenchmarkRequest);
                nonConcurrentBenchmark = int.Parse(Console.ReadLine());
                Console.WriteLine(concurrentBulkRequest);
                concurrentBulk = int.Parse(Console.ReadLine());
                return 0;
            }
        }

        private static List<KeyValuePair<string, int>> GenerateRandomKeyValuePairs(int treeDepth, int numOfKeys)
        {
            List<KeyValuePair<string, int>> generatedRandomKeysValues = new List<KeyValuePair<string, int>>();
            Random rnd = new Random();
            List<string> generatedRandomKeys = new List<string>();
            int[] seenLen = new int[maxTreeDepth];
            int indx = 0;
            generatedRandomKeys.AddRange(RandomStringFilesGenerator(treeDepth, numOfKeys));

            if (generatedRandomKeys.Count != numOfKeys)
            {
                Console.WriteLine("numofpairs: " + generatedRandomKeys.Count);
            }

            foreach (string key in generatedRandomKeys)
            {
                generatedRandomKeysValues.Add(new KeyValuePair<string, int>(key, rnd.Next(1, int.MaxValue)));
            }

            return generatedRandomKeysValues;
        }

        private static List<KeyValuePair<string, int>>[] DivideRandomKeyValuePairsToThreads(List<KeyValuePair<string, int>> allKeyValuePairs, int numOfThreads)
        {
            List<KeyValuePair<string, int>>[] divideRandomKeyValuePairsToThreads = new List<KeyValuePair<string, int>>[numOfThreads];
            int randLength;
            int m = 1;
            Random random = new Random();
            int[] pointers = new int[numOfThreads];
            if (numOfInserts >= numOfRemovals)
            {
                pointers[0] = numOfInserts;
                while (m < numOfThreads)
                {
                    try
                    {
                        randLength = random.Next(1, numOfInserts);
                        if (pointers.Contains(randLength)) continue;
                        pointers[m] = randLength;
                        m++;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exception: " + numOfInserts);
                    }
                }

                Array.Sort(pointers);
                int count = 0;
                for (int i = 0; i < numOfThreads; i++)
                {
                    divideRandomKeyValuePairsToThreads[i] = allKeyValuePairs.GetRange(count, pointers[i] - count);
                    count = pointers[i];
                }
            }
            else
            {
                int avgDeletionsPerthread = numOfRemovals / numOfThreads;
                numOfInsertionThreads = numOfInserts / avgDeletionsPerthread;
                if (numOfInsertionThreads > numOfThreads)
                {
                    numOfInsertionThreads = numOfThreads;
                }
                pointers[0] = numOfInserts;
                while (m < numOfInsertionThreads)
                {
                    randLength = random.Next(1, numOfInserts);
                    if (pointers.Contains(randLength)) continue;
                    pointers[m] = randLength;
                    m++;
                }
                if (m < numOfThreads)
                {
                    pointers[m] = numOfRemovals;
                    m++;
                }
                Array.Sort(pointers);

                int curr = 0;
                while (m < numOfThreads && curr < numOfThreads - numOfInsertionThreads)
                {
                    randLength = random.Next(numOfInserts, numOfRemovals);
                    if (pointers.Contains(randLength)) continue;
                    pointers[curr] = randLength;
                    m++;
                    curr++;
                }

                Array.Sort(pointers);
                int count = 0;
                for (int i = 0; i < numOfThreads; i++)
                {
                    divideRandomKeyValuePairsToThreads[i] = allKeyValuePairs.GetRange(count, pointers[i] - count);
                    count = pointers[i];
                }
            }
            return divideRandomKeyValuePairsToThreads;
        }

        private static List<KeyValuePair<string, int>>[] DivideRandomKeyValuePairsToThreadsNM(List<KeyValuePair<string, int>> allKeyValuePairs, int numOfThreads)
        {
            List<KeyValuePair<string, int>>[] divideRandomKeyValuePairsToThreads = new List<KeyValuePair<string, int>>[numOfThreads];
            int randLength;
            int m = 1;
            Random random = new Random();
            int[] pointers = new int[numOfThreads];
            pointers[0] = allKeyValuePairs.Count;
            while (m < numOfThreads)
            {
                try
                {
                    randLength = random.Next(1, allKeyValuePairs.Count);
                    if (pointers.Contains(randLength)) continue;
                    pointers[m] = randLength;
                    m++;
                }
                catch (Exception e)
                {
                    break;
                }
            }

            Array.Sort(pointers);
            int count = 0;
            for (int i = 0; i < numOfThreads; i++)
            {
                divideRandomKeyValuePairsToThreads[i] = allKeyValuePairs.GetRange(count, pointers[i] - count);
                count = pointers[i];
            }

            return divideRandomKeyValuePairsToThreads;
        }
        public static int FindNumInArray(List<KeyValuePair<string, int>>[] arr)
        {
            for (int i = 0; i < arr.Count(); i++)
            {
                if (arr[i].Count == numOfInserts) return i;
            }
            return -1;
        }

        private static void StartConcurrentMeasurementsWithNumberOfThreads(List<KeyValuePair<string, int>> keyValuePairs, int numOfThreads, int index)
        {
            keyValuesByThreadsFG = DivideRandomKeyValuePairsToThreads(keyValuePairs, numOfThreads);
            keyValuesByThreadsCG = new List<KeyValuePair<string, int>>[keyValuesByThreadsFG.Count()];
            concurrentLL = new RadixTree_FineGrained.CLL<int>[keyValuesByThreadsFG.Count()];
            concurrentLLI = new RadixTree_FineGrained.CLL<int>[keyValuesByThreadsFG.Count()];

            int newIn = 0;
            foreach (List<KeyValuePair<string, int>> lst in keyValuesByThreadsFG)
            {
                concurrentLLI[newIn] = new RadixTree_FineGrained.CLL<int>(lst);
                concurrentLL[newIn] = new RadixTree_FineGrained.CLL<int>(lst.GetRange(0, (int)Math.Floor(lst.Count * removalFactor)));
                keyValuesByThreadsCG[newIn] = lst.GetRange(0, lst.Count);
                newIn++;
            }
            Thread[] measuredThreadsFG, measuredThreadsCG;
            if (numOfInserts >= numOfRemovals)//#1 Thread case
            {
                removalFactor = numOfRemovals / numOfInserts;
                measuredThreadsFG = new Thread[numOfThreads];
                measuredThreadsCG = new Thread[numOfThreads];
                int sumcheckup = 0;
                if (numOfInserts > 0 && numOfInsertionThreads == 0)
                {
                    numOfInsertionThreads++;
                }

                for (int i = 0; i < numOfThreads; i++)
                {
                    sumcheckup += keyValuesByThreadsFG[i].Count;
                    measuredThreadsFG[i] = new Thread(new ParameterizedThreadStart(MoreInsertionThanDeletesThreadFG));
                    measuredThreadsCG[i] = new Thread(new ParameterizedThreadStart(MoreInsertionThanDeletesThreadCG));
                }


                StartFGConcurrentMeasurements(keyValuePairs.GetRange(numOfInserts, numberOfKeys - numOfInserts), measuredThreadsFG, index);
                StartCGConcurrentMeasurements(keyValuePairs.GetRange(numOfInserts, numberOfKeys - numOfInserts), measuredThreadsCG, index);
            }

            else // #2 thread case
            {
                removalFactor = 1;
                measuredThreadsFG = new Thread[numOfThreads];
                measuredThreadsCG = new Thread[numOfThreads];
                int sumcheckup = 0;
                if (numOfInserts > 0 && numOfInsertionThreads == 0)
                {
                    numOfInsertionThreads++;
                }
                for (int i = 0; i < numOfInsertionThreads; i++)
                {
                    sumcheckup += keyValuesByThreadsFG[i].Count;
                    measuredThreadsFG[i] = new Thread(new ParameterizedThreadStart(MoreInsertionThanDeletesThreadFG));
                    measuredThreadsCG[i] = new Thread(new ParameterizedThreadStart(MoreInsertionThanDeletesThreadCG));
                }

                if (sumcheckup != numOfInserts)
                {
                    Console.WriteLine("sumcheckup: " + sumcheckup + " numofinserts: " + numOfInserts);
                }

                for (int i = numOfInsertionThreads; i < numOfThreads; i++)
                {
                    sumcheckup += keyValuesByThreadsFG[i].Count;
                    measuredThreadsFG[i] = new Thread(new ParameterizedThreadStart(OnlyDeletionThreadFG));
                    measuredThreadsCG[i] = new Thread(new ParameterizedThreadStart(OnlyDeletionThreadCG));
                }

                StartFGConcurrentMeasurements(keyValuePairs.GetRange(numOfInserts, numberOfKeys - numOfInserts), measuredThreadsFG, index);
                StartCGConcurrentMeasurements(keyValuePairs.GetRange(numOfInserts, numberOfKeys - numOfInserts), measuredThreadsCG, index);
            }
        }

        private static Dictionary<KeyValuePair<string, int>, int> InitNewDictionary(List<KeyValuePair<string, int>> lst)
        {
            Dictionary<KeyValuePair<string, int>, int> dict = new Dictionary<KeyValuePair<string, int>, int>();
            foreach (KeyValuePair<string, int> kvp in lst)
            {
                dict.Add(kvp, 0);
            }

            return dict;
        }

        private static void PrintTimeSpans()
        {
            Console.WriteLine("First printing non concurrent Tree");
            Console.WriteLine("Minutes: " + nonCCtime.Minutes + " Seconds: " + nonCCtime.Seconds + " Miliseconds: " + nonCCtime.Milliseconds);
            Console.WriteLine("Now printing all CG tree times");
            int i = 0;
            int numT;
            TimeSpan ts2;
            foreach (TimeSpan ts in CGTime)
            {
                if (i >= numberOfThreadsInCurrentMeasurement.Count())
                {
                    break;
                }
                numT = numberOfThreadsInCurrentMeasurement[i];
                ts2 = FGTime[i];
                if (ts != null && ts2 != null)
                {
                    Console.WriteLine("CG lock with " + numT + " Threads. Minutes: " + ts.Minutes + " Seconds: " + ts.Seconds + " Miliseconds: " + ts.Milliseconds);
                    Console.WriteLine("FG lock with " + numT + " Threads. Minutes: " + ts2.Minutes + " Seconds: " + ts2.Seconds + " Miliseconds: " + ts2.Milliseconds);
                }
                else
                {
                    break;
                }
                i++;
            }
        }
        private static void StartNonConcurrentTreeMeasurement(List<KeyValuePair<string, int>> lst)
        {
            Stopwatch sw = new Stopwatch();
            for (int i = lst.Count - 1; i >= 0; i--)
            {
                if (nonCCTree.Size() == (numberOfKeys - numOfInserts))
                {
                    sw.Start();
                }

                nonCCTree.Insert(lst[i].Key, lst[i].Value);
            }

            int k = lst.Count - 1;
            while (nonCCTree.Size() > (numberOfKeys - numOfRemovals))
            {
                nonCCTree.Remove(lst[k].Key);
                k--;
            }

            sw.Stop();
            nonCCtime = sw.Elapsed;
            while (nonCCTree.Size() > 0)
            {
                nonCCTree.Remove(lst[k].Key);
                k--;
            }
        }

        private static void StartCGConcurrentMeasurements(List<KeyValuePair<string, int>> lst, Thread[] measuredThreads, int index)
        {
            Console.WriteLine("Starting CG with " + measuredThreads.Count() + " Threads");
            lsts = DivideRandomKeyValuePairsToThreadsNM(lst, measuredThreads.Count());
            InsertWithoutMeasurementCG();
            MeasureThreadsCG(measuredThreads, index);
            Console.WriteLine("Finished test CG with " + measuredThreads.Count() + " Threads. Time: " + CGTime[index]);

        }

        private static void StartFGConcurrentMeasurements(List<KeyValuePair<string, int>> lst, Thread[] measuredThreads, int index)
        {
            Console.WriteLine("Starting FG with " + measuredThreads.Count() + " Threads");
            lsts = DivideRandomKeyValuePairsToThreadsNM(lst, measuredThreads.Count());
            InsertWithoutMeasurementFG();
            MeasureThreadsFG(measuredThreads, index);
            Console.WriteLine("Finished test FG with " + measuredThreads.Count() + " Threads. Time: " + FGTime[index]);

        }

        private static void InsertWithoutMeasurementCG()
        {
            var measuredThreadsCG = new Thread[lsts.Count()];
            for (int i = 0; i < lsts.Count(); i++)
            {
                measuredThreadsCG[i] = new Thread(new ParameterizedThreadStart(OnlyInsertionThreadCG));
            }

            int k = 0;
            foreach (Thread t in measuredThreadsCG)
            {
                t.Start(k);
                k++;
            }

            foreach (Thread t in measuredThreadsCG)
            {
                t.Join();
            }
        }

        private static void InsertWithoutMeasurementFG()
        {
            var measuredThreadsFG = new Thread[lsts.Count()];
            for (int i = 0; i < lsts.Count(); i++)
            {
                measuredThreadsFG[i] = new Thread(new ParameterizedThreadStart(OnlyInsertionThreadFG));
            }

            int k = 0;
            foreach (Thread t in measuredThreadsFG)
            {
                t.Start(k);
                k++;
            }

            foreach (Thread t in measuredThreadsFG)
            {
                t.Join();
            }
        }

        private static void DeleteWithoutMeasurementCG(List<KeyValuePair<string, int>> lst)
        {
            foreach (KeyValuePair<string, int> kvp in lst)
            {
                cgTree.Remove(kvp.Key);
            }
        }

        private static void MeasureThreadsFG(Thread[] measuredThreads, int index)
        {
            int i = 0;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (Thread t in measuredThreads)
            {
                t.Start(i);
                i++;
            }
            foreach (Thread t in measuredThreads)
            {
                t.Join();
            }

            sw.Stop();
            FGTime[index] = sw.Elapsed;

        }

        private static void MeasureThreadsCG(Thread[] measuredThreads, int index)
        {
            int i = 0;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (Thread t in measuredThreads)
            {
                t.Start(i);
                i++;
            }
            foreach (Thread t in measuredThreads)
            {
                t.Join();
            }

            sw.Stop();
            CGTime[index] = sw.Elapsed;
        }

        public static void MoreInsertionThanDeletesThreadFG(object index)
        {
            int indx = (int)index;
            if (concurrentBulk == 1)
            {
                fgTree.BULKInsert(concurrentLLI[indx]);
                fgTree.BULKRemove(concurrentLL[indx]);
            }

            else
            {
                var lst = keyValuesByThreadsFG[indx];
                int numOfRemovalFactor = (int)Math.Floor(lst.Count * removalFactor);
                int i = 0;
                foreach (KeyValuePair<string, int> kvp in lst)
                {
                    fgTree.Insert(kvp.Key, kvp.Value);
                }
                foreach (KeyValuePair<string, int> kvp in lst)
                {
                    if (i >= numOfRemovalFactor) break;
                    fgTree.Remove(kvp.Key);
                    i++;
                }
            }
        }
        public static void MoreInsertionThanDeletesThreadCG(object index)
        {
            int indx = (int)index;
            var lst = keyValuesByThreadsCG[indx];
            int numOfRemovalFactor = (int)Math.Floor(lst.Count * removalFactor);
            foreach (KeyValuePair<string, int> kvp in lst)
            {
                cgTree.Insert(kvp.Key, kvp.Value);
            }

            int i = 0;
            foreach (KeyValuePair<string, int> kvp in lst)
            {
                if (i >= removalFactor) break;
                cgTree.Remove(kvp.Key);
                i++;
            }
        }

        public static void OnlyDeletionThreadFG(object index)
        {
            int indx = (int)index;
            if (concurrentBulk == 1)
            {
                fgTree.BULKRemove(concurrentLL[indx]);
            }

            else
            {
                var lst = keyValuesByThreadsFG[indx];
                foreach (KeyValuePair<string, int> kvp in lst)
                {
                    fgTree.Remove(kvp.Key);
                }
            }
        }
        public static void OnlyDeletionThreadCG(object index)
        {
            int indx = (int)index;
            var lst = keyValuesByThreadsCG[indx];
            foreach (KeyValuePair<string, int> kvp in lst)
            {
                cgTree.Remove(kvp.Key);
            }
        }

        public class MeasurementsJson
        {
            public int NumberOfKeys { get; set; }
            public int PercantageRemoval { get; set; }
            public int PercantageInserts { get; set; }
            public int NumberOfInserts { get; set; }
            public int NumberOfRemovals { get; set; }
            public int TreeDepth { get; set; }

            public int NumberOfThreads { get; set; }

            public int IsBulk { get; set; }
            public int NonConcurrentMeasured { get; set; }
            
            public string NonConcurrentResult { get; set; }
            public string CourseGrainedLockResult { get; set; }
            public string FineGrainedLockResult
            {
                get; set;
            }
        }


            public static void OnlyInsertionThreadFG(object index)
            {
                int indx = (int)index;
                var lst = lsts[indx];
                foreach (KeyValuePair<string, int> kvp in lst)
                {
                    fgTree.Insert(kvp.Key, kvp.Value);
                }
            }

            public static void OnlyInsertionThreadCG(object index)
            {
                int indx = (int)index;
                var lst = lsts[indx];
                foreach (KeyValuePair<string, int> kvp in lst)
                {
                    cgTree.Insert(kvp.Key, kvp.Value);
                }
            }
        }
    
}
