﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace RadixTree_FineGrained
{
    public class CRT<T>
    {
        private static readonly Object obj = new Object();

        public volatile Node<T> root;
        public Object locker = new Object();
        public SearchEntities_Factory<T> searchEntityFactory;

        public CRT()
        {
            root = new Node<T>(default(T), "", new List<Node<T>>(), true);
            searchEntityFactory = new SearchEntities_Factory<T>(this);
        }

        public IEnumerable<T> SearchChildrenValues(string startKey, Node<T> startNode)
        {
            return new Enumerators<T>.BaseValuesEnumerable(startNode, startKey);
        }

        public T SearchValueOfSpecificKey(string key)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
            return result.GetValueForKeyFound();
        }

        public IEnumerable<string> SearchKeysAccordingToPrefix(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetKeysWithGivenPrefix();
        }

        public IEnumerable<T> SearchValuesOfKeysAccordingToPrefix(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetValuesOfKeysWithGivenPrefix();
        }

        public IEnumerable<KeyValuePair<string, T>> SearchKeyValuePairsAccordingToPrefix(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetKeyValuePairsOfKeysWithGivenPrefix();
        }        

        public IEnumerable<string> SearchSimilarKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetKeysWithLongestGivenPrefix();
        }

        public IEnumerable<T> SearchValuesOfSimilarKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetValuesOfKeysWithLongestGivenPrefix();
        }

        public IEnumerable<KeyValuePair<string, T>> SearchKeyValuePairsOfSimilarKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetKeyValuePairsOfKeysWithLongestGivenPrefix();
        }

        public int Size()
        {         
            LinkedList<Node<T>> stack = new LinkedList<Node<T>>();
            stack.AddFirst(root);
            int count = 0;
            while (true)
            {
                if (stack.Count == 0)
                {
                    return count;
                }
                Node<T> current = stack.First();
                stack.RemoveFirst();
                stack.AddAll(current.GetOutGoingEdgesList());
                if (!current.GetValue().Equals(default(T)))
                {
                    count++;
                }
            }
        }        

        public IEnumerable<string> SearchChildernKeys(string startKey, Node<T> startNode)
        {
            return (IEnumerable<string>)(new Enumerators<T>.BaseKeysEnumerable<string>(startNode, startKey));
        }

        public IEnumerable<KeyValuePair<string, T>> SearchChildernKeyValuePairs(string startKey, Node<T> startNode)
        {
            return (IEnumerable<KeyValuePair<string, T>>)new Enumerators<T>.BaseKeyValuePairsEnumerable(startNode, startKey);
        }

        public IEnumerable<NodeKeyPair<T>> TraverseChildren(string startKey, Node<T> startNode)
        {
            return new Enumerators<T>.BaseTreeTraverseEnumerable(startNode, startKey);
        }

        public T Insert(string key, T value, bool overwrite = true)
        {
            if (key == null)
            {
                throw new InnerExceptions.KeyIsNullException();
            }
            if (key.Length == 0)
            {
                throw new InnerExceptions.KeyInsertedOfZeroLengthException();
            }
            if (value.Equals(default(T)))
            {
                throw new InnerExceptions.ValueInsertedhDefaultException();
            }

            try
            {
                T res = default(T);
                int k = 0;
                while(true)
                {
                    SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                    SearchEntities<T>.ValidationState insertResult = result.InsertSearchEntities(overwrite, value, ref res);
                    if (insertResult.Equals(SearchEntities<T>.ValidationState.SUCCESS))
                    {
                        return res;
                    }
                    k++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return default(T);
        }                

        public bool Remove(string key)
        {
            if (key == null)
            {
                throw new InnerExceptions.KeyIsNullException();
            }

            try
            {
                while (true)
                {
                    SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                    SearchEntities<T>.ValidationState removeResult = result.RemoveSearchEntities();
                    if (removeResult.Equals(SearchEntities<T>.ValidationState.FAILURE))
                    {
                        return false;
                    }
                    else if (removeResult.Equals(SearchEntities<T>.ValidationState.SUCCESS))
                    {
                        return true;
                    }

                }
            }
            catch (Exception)
            {
                Console.WriteLine("Exception during search entity creation");
            }

            return false;
        }

        public void GetTreeDepth()
        {
            Console.WriteLine(MyStringBuilder<T>.BuildStringFromTree(this));
        }
 
        public bool BULKInsert(CLL<T> lst, bool overwrite = true)
        {
            KeyValuePair<string, T> kvp;
            T value;
            string key;
            CLL<T>.INode current, prev;
            try
            {
                current = lst.head;
                while (lst.head != null)
                {
                    kvp = current.me;
                    key = kvp.Key;
                    value = kvp.Value;
                    if (key == null)
                    {
                        throw new InnerExceptions.KeyIsNullException();
                    }
                    if (key.Length == 0)
                    {
                        throw new InnerExceptions.KeyInsertedOfZeroLengthException();
                    }
                    if (value.Equals(default(T)))
                    {
                        throw new InnerExceptions.ValueInsertedhDefaultException();
                    }

                    T res = default(T);
                    SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                    SearchEntities<T>.ValidationState insertResult = result.InsertSearchEntities(overwrite, value, ref res);
                    if (!insertResult.Equals(SearchEntities<T>.ValidationState.KEEP_GOING))
                    {
                        prev = current;
                        current = current.next;
                        lst.Remove(prev);
                        continue;
                    }

                    if (current.next != null)
                    {
                        current = current.next;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;

        }

        public bool BULKRemove(CLL<T> lst)
        {
            KeyValuePair<string, T> kvp;
            T value;
            string key;
            CLL<T>.INode current, prev;
            try
            {
                current = lst.head;
                while (lst.head != null)
                {
                    kvp = current.me;
                    key = kvp.Key;
                    value = kvp.Value;
                    SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                    SearchEntities<T>.ValidationState removeResult = result.RemoveSearchEntities();
                    if (!removeResult.Equals(SearchEntities<T>.ValidationState.KEEP_GOING))
                    {
                        prev = current;
                        current = current.next;
                        lst.Remove(prev);
                        continue;
                    }

                    if (current.next!=null)
                    {
                        current = current.next;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception during search entity creation");
                return false;
            }  
       
            return true;
        }
    }
    public class CLL<T>
    {
        public class INode
        {
            public KeyValuePair<string, T> me { get; set; }
            public INode next { get; set; }
            public INode prev { get; set; }

            public INode(KeyValuePair<string, T> me, INode prev = null, INode next = null)
            {
                this.me = me;
                this.next = next;
                this.prev = prev;
            }
        }

        public INode head { get; set; }
        public CLL(List<KeyValuePair<string, T>> lst)
        {
            INode current = null, prev = null, next = null;
            foreach (KeyValuePair<string, T> elem in lst)
            {
                if (head == null)
                {
                    head = new INode(elem);
                    current = head;
                    continue;
                }

                if (current == head)
                {
                    next = new INode(elem, current, current);
                    current.next = next;
                    current.prev = next;
                    current = next;
                    continue;
                }

                next = new INode(elem, current, current.next);
                current.next.prev = next;
                current.next = next;
            }
        }

        public void Remove(INode node)
        {
            if (node==null)
            {
                throw new Exception("Illegal remove of null");
            }

            INode next = node.next;
            INode prev = node.prev;
            if (next != null && prev != null && next != prev)
            {
                next.prev = prev;
                prev.next = next;
                if (node == head)
                {
                    head = next;
                }
            }

            else if (next != null && next == prev)
            {
                if (node == head)
                {
                    head = node.next;
                    head.next = null;
                    head.prev = null;
                }

                else
                {
                    if (node.prev != head)
                    {
                        throw new Exception("illegal state");
                    }
                    head.next = null;
                    head.prev = null;
                }
            }

            else if (next == null & prev == null)
            {
                head = null;
            }
        }
    }
}