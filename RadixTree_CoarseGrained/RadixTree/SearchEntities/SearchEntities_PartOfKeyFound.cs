﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixTree
{
    public class SearchEntities_PartOfKeyFound<T> : SearchEntities<T>
    {
        public SearchEntities_PartOfKeyFound(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {

        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<string>();
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            if (totalNumOfEqualChars == 0)
            {
                // Closest match is the root node, we don't consider this a match for anything...
                return new Enumerators<T>.EmptyBaseEnumerable<string>();
            }
            // Example: if we searched for COFFEE, but deepest matching node was CO,
            // the results should include node CO and its descendants...
            string keyOfNodeFound = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars);
            return currentTree.GetDescendantKeys(keyOfNodeFound, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
        }
        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {
            if (totalNumOfEqualChars == 0)
            {
                // Closest match is the root node, we don't consider this a match for anything...
                return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
            }
            // Example: if we searched for COFFEE, but deepest matching node was CO,
            // the results should include node CO and its descendants...
            string keyOfNodeFound = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars);
            return currentTree.GetDescendantKeyValuePairs(keyOfNodeFound, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return default(T);
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<T>();
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {
            if (totalNumOfEqualChars == 0)
            {
                // Closest match is the root node, we don't consider this a match for anything...
                return new Enumerators<T>.EmptyBaseEnumerable<T>();
            }
            // Example: if we searched for COFFEE, but deepest matching node was CO,
            // the results should include node CO and its descendants...
            string keyOfNodeFound = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars);
            return currentTree.GetDescendantValues(keyOfNodeFound, resultNode);
        }

        public override T Insert(bool overwrite, T value)
        {
            // Search found a difference in characters between the key and the start of all child edges leaving the
            // node, the key still has trailing unmatched characters.
            // -> Add a new child to the node, containing the trailing characters from the key.

            // NOTE: this is the only branch which allows an edge to be added to the root.
            // (Root node's own edge is "" empty string, so is considered a prefixing edge of every key)

            // Create a new child node containing the trailing characters...
            string keySuffix = key.Substring(totalNumOfEqualChars);
            Node<T> newChild = new Node<T>(value, keySuffix, new List<Node<T>>(), false);

            // Clone the current node adding the new child...
            List<Node<T>> edges = new List<Node<T>>(resultNode.GetOutEdgesList().Size() + 1);
            edges.AddAll(resultNode.GetOutEdgesList());
            edges.Add(newChild);
            Node<T> clonedNode = new Node<T>(resultNode.GetValue(), resultNode.GetInEdges(), edges, resultNode == currentTree.root);

            // Re-add the cloned node to its parent node...
            if (resultNode == currentTree.root)
            {
                currentTree.root = clonedNode;
            }
            else
            {
                parent.UpdateOutEdgeByChildNode(clonedNode);
            }

            // Return null for the existing value...
            return default(T);
        }

        public override bool Remove()
        {
            return false;
        }
    }
}
