﻿using System;
using System.Collections.Generic;


namespace RadixTree
{
    public class ConcurrentArrayAsList<T> : List<T>
    {
        private ConcurrentArray<T> concurrentArray;

        public ConcurrentArrayAsList(ConcurrentArray<T> concurrentArray)
        {
            this.concurrentArray = concurrentArray;
        }

        public int GetSize
        {
            get { return concurrentArray.size; }
        }

        public new T[] ToArray()
        {
            return concurrentArray.ToArray();
        }

        public new T this[int index]
        {
            get
            {
                if (index > concurrentArray.size)
                {
                    return default(T);
                }

                return concurrentArray.Get(index);
            }

            set
            {
                concurrentArray.ConcurrentSet(index, value);
            }
        }

        public override string ToString()
        {
            return concurrentArray.ToString();
        }
    }
}
