﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RadixTree_CoarseGrained;

namespace RadixTreeTest_CoarseGrained
{
    [TestClass]
    public class UnitTest
    {
        string nodeSign = ((char)0x0298).ToString();

        [TestMethod]
        public void Test_01_BuildNaiveTree()
        {
            CRT<int> tree = new CRT<int>();
          
            tree.Insert("HELLO", 1);
            String staticTreeResult =
                    nodeSign + "\n" +
                    "└── " + nodeSign + " [HELLO, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_02_SetValue()
        {
            CRT<int> tree = new CRT<int>();

            int result;
            result = tree.Insert("A", 1);
            Assert.IsTrue(int.Equals(result, 0));
            result = tree.Insert("A", 2);
            Assert.IsTrue(int.Equals(result, 1));

            Assert.IsTrue(Equals(2, tree.SearchValueOfSpecificKey("A")));
        }

        [TestMethod]
        public void Test_03_CheckNodeSorting()
        {    
            CRT<int> tree = new CRT<int>();
            tree.Insert("B", 1);
            tree.Insert("A", 2);
            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "├── " + nodeSign + " [A, 2]\n" +
                    "└── " + nodeSign + " [B, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_04_AppendSubstringNode()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 1]\n" +
                    "    └── " + nodeSign + " [LLO, 2]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_05_CheckNodeSplitting()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO", 1);
            tree.Insert("HE", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 2]\n" +
                    "    └── " + nodeSign + " [LLO, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_06_AppendNodeCommonPrefix()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " HE\n" + 
                    "    ├── " + nodeSign + " [LLO, 1]\n" +
                    "    └── " + nodeSign + " [RE, 2]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_07_AppendNodeCommonPrefixComplex()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +             
                    "    ├── " + nodeSign + " E\n" +      
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }  

        [TestMethod]
        public void Test_08_OverrideValue()
        {
            CRT<int> tree = new CRT<int>();
            int result;

            result = tree.Insert("A", 1);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.Insert("A", 2);
            Assert.IsTrue(int.Equals(result, 1));

            Assert.IsTrue(Equals(1, tree.SearchValueOfSpecificKey("A")));
        }

        [TestMethod]
        public void Test_09_Insert_And_SplitNode()
        {
            CRT<int> tree = new CRT<int>();
            int result;

            result = tree.Insert("HELLO", 1);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.Insert("HERE", 1);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.Insert("HE", 2);
            Assert.IsTrue(int.Equals(result, 0));
        }

        [TestMethod]
        public void Test_10_keyValidation()
        {
            CRT<int> tree = new CRT<int>();
            try
            {
                tree.Insert(null, 1);
            }
            catch(Exception)
            {
                Console.WriteLine("Got the exception staticTreeResult");   
            }
        }

        [TestMethod]
        public void Test_11_valueValidation()
        {
            CRT<int> tree = new CRT<int>();
            try
            {
                tree.Insert("A", 0);
            }
            catch (Exception)
            {
                Console.WriteLine("Got the exception staticTreeResult");
            }
        }

        [TestMethod]
        public void Test_12_BuildComplexString()
        {
            CRT<int> tree = new CRT<int>();
            tree.Insert("HELLO_MY_FRIEND", 6);
            tree.Insert("HELLO_MY", 5);
            tree.Insert("HELLO_THERE", 4);
            tree.Insert("HELLO_", 3);
            tree.Insert("HE", 2);
            tree.Insert("H", 1);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [H, 1]\n" +
                    "    └── " + nodeSign + " [E, 2]\n" +
                    "        └── " + nodeSign + " [LLO_, 3]\n" +
                    "            ├── " + nodeSign + " [MY, 5]\n" +
                    "            │   └── " + nodeSign + " [_FRIEND, 6]\n" +
                    "            └── " + nodeSign + " [THERE, 4]\n";

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_13_CheckSize()
        {
           CRT<int> tree = new CRT<int>();

           Assert.IsTrue(int.Equals(0, tree.Size()));

           tree.Insert("HELLO", 1);
           Assert.IsTrue(int.Equals(1, tree.Size()));

           tree.Insert("HERE", 2);
           Assert.IsTrue(int.Equals(2, tree.Size()));

           tree.Insert("HOUSE", 3);
           Assert.IsTrue(int.Equals(3, tree.Size()));
       
           tree.Remove("B");
           Assert.IsTrue(int.Equals(3, tree.Size()));

           tree.Remove("HOUSE");
           Assert.IsTrue(int.Equals(2, tree.Size()));

           tree.Remove("HERE");
           Assert.IsTrue(int.Equals(1, tree.Size()));

           tree.Remove("HELLO");
           Assert.IsTrue(int.Equals(0, tree.Size()));
        }

        [TestMethod]
        public void Test_14_CheckGetValueForKey()
        {
           CRT<int> tree = new CRT<int>();

           tree.Insert("HELLO", 1);
           tree.Insert("HERE", 2);
           tree.Insert("HOUSE", 3);

           Assert.IsTrue(int.Equals(1, tree.SearchValueOfSpecificKey("HELLO")));
           Assert.IsTrue(int.Equals(2, tree.SearchValueOfSpecificKey("HERE")));
           Assert.IsTrue(int.Equals(3, tree.SearchValueOfSpecificKey("HOUSE")));
           Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("H")));
           Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("HE")));
           Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("E")));
           Assert.IsTrue(int.Equals(0, tree.SearchValueOfSpecificKey("")));
        }

        [TestMethod]
        public void Test_15_Remove_MoreThanOneChildEdge()
        { 
           CRT<int> tree = new CRT<int>();

           tree.Insert("H", 1);
           tree.Insert("HELLO", 2);
           tree.Insert("HAM", 3);

           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [H, 1]\n" +
                   "    ├── " + nodeSign + " [AM, 3]\n" +
                   "    └── " + nodeSign + " [ELLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("H");
           Assert.IsTrue(Removed);

           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " H\n" +
                   "    ├── " + nodeSign + " [AM, 3]\n" +
                   "    └── " + nodeSign + " [ELLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_16_Remove_ExactlyOneChildEdge()
        {
           CRT<int> tree = new CRT<int>();

           tree.Insert("H", 1);
           tree.Insert("HELLO", 2);
           tree.Insert("HELLO_WORLD", 3);
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [H, 1]\n" +
                   "    └── " + nodeSign + " [ELLO, 2]\n" +
                   "        └── " + nodeSign + " [_WORLD, 3]\n";
           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("H");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HELLO, 2]\n" +
                   "    └── " + nodeSign + " [_WORLD, 3]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_17_Remove_ZeroChildEdges_DirectChildOfRoot()
        {
           CRT<int> tree = new CRT<int>();

           tree.Insert("A", 1);
           tree.Insert("B", 2);
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "├── " + nodeSign + " [A, 1]\n" +
                   "└── " + nodeSign + " [B, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("A");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [B, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_18_Remove_LastRemainingKey()
        {
           CRT<int> tree = new CRT<int>();

           tree.Insert("A", 5);
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [A, 5]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("A");
           Assert.IsTrue(Removed);
      
           staticTreeResult =
                   "" + nodeSign + "\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_19_Remove_ZeroChildEdges_OneStepFromRoot()
        { 
           CRT<int> tree = new CRT<int>();

           tree.Insert("HE", 1);
           tree.Insert("HELLO", 2);  
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n" +
                   "    └── " + nodeSign + " [LLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("HELLO");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_20_Remove_ZeroChildEdges_SeveralStepsFromRoot()
        { 
           CRT<int> tree = new CRT<int>();

           tree.Insert("HE", 1);
           tree.Insert("HELLO", 2);
           tree.Insert("HELLO_WORLD", 3);

           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n" +
                   "    └── " + nodeSign + " [LLO, 2]\n" +
                   "        └── " + nodeSign + " [_WORLD, 3]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("HELLO_WORLD");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n" +
                   "    └── " + nodeSign + " [LLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_21_testRemove_DoNotRemoveSplitNode()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Insert("HELLO", 1);
            radixTree.Insert("HERE", 2);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " HE\n" +
                    "    ├── " + nodeSign + " [LLO, 1]\n" +
                    "    └── " + nodeSign + " [RE, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HE");
            Assert.IsFalse(Removed);

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_22_testRemove_MergeSplitNode()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Insert("HELLO", 1);
            radixTree.Insert("HERE", 2);
            radixTree.Insert("HOUSE", 3);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " E\n" +
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HELLO");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [ERE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_23_testRemove_DoNotMergeSplitNodeWithValue()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Insert("HELLO", 1);
            radixTree.Insert("HERE", 2);
            radixTree.Insert("HOUSE", 3);
            radixTree.Insert("HE", 4);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [E, 4]\n" +
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HELLO");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [E, 4]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        public void TEST_24_testRemove_NoSuchKey()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("A", 1);
            tree.Insert("B", 2);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "├── " + nodeSign + " [A, 1]\n" +
                    "└── " + nodeSign + " [B, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("HELLO");
            Assert.IsFalse(Removed);

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_25_testGetKeysForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);
            tree.Insert("HER", 4);
            tree.Insert("A", 5);

            Assert.IsTrue(String.Equals("[A, HELLO, HER, HERE, HOUSE]", ToString(tree.SearchKeysAccordingToPrefix(""))));
            Assert.IsTrue(String.Equals("[A]", ToString(tree.SearchKeysAccordingToPrefix("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchKeysAccordingToPrefix("AB"))));
            Assert.IsTrue(String.Equals("[HELLO, HER, HERE, HOUSE]", ToString(tree.SearchKeysAccordingToPrefix("H"))));
            Assert.IsTrue(String.Equals("[HELLO, HER, HERE]", ToString(tree.SearchKeysAccordingToPrefix("HE"))));
            Assert.IsTrue(String.Equals("[HER, HERE]", ToString(tree.SearchKeysAccordingToPrefix("HER"))));
            Assert.IsTrue(String.Equals("[HOUSE]", ToString(tree.SearchKeysAccordingToPrefix("HO"))));
        }

        [TestMethod]
        public void TEST_26_CheckGetClosestKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HAM", 3);

            Assert.IsTrue(String.Equals("[HAM, HE, HELLO]", ToString(tree.SearchSimilarKeys("HI"))));
            Assert.IsTrue(String.Equals("[HE, HELLO]", ToString(tree.SearchSimilarKeys("HE"))));
            Assert.IsTrue(String.Equals("[HELLO]", ToString(tree.SearchSimilarKeys("HELL"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchSimilarKeys("B"))));
            Assert.IsTrue(String.Equals("[HELLO]", ToString(tree.SearchSimilarKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_27_CheckGetValuesForClosestKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HAM", 3);

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);

            Assert.IsTrue(String.Equals("[3, 1, 2]", ToString(tree.SearchValuesOfSimilarKeys("HI"))));
            Assert.IsTrue(String.Equals("[1, 2]", ToString(tree.SearchValuesOfSimilarKeys("HE"))));
            Assert.IsTrue(String.Equals("[3]", ToString(tree.SearchValuesOfSimilarKeys("HA"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchValuesOfSimilarKeys("B"))));
            Assert.IsTrue(String.Equals("[2]", ToString(tree.SearchValuesOfSimilarKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_28_CheckGetKeyValuePairsForClosestKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HE", 1);
            tree.Insert("HELLO", 2);
            tree.Insert("HAM", 3);

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);

            Assert.IsTrue(String.Equals("[[HAM, 3], [HE, 1], [HELLO, 2]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HI"))));
            Assert.IsTrue(String.Equals("[[HE, 1], [HELLO, 2]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HE"))));
            Assert.IsTrue(String.Equals("[[HAM, 3]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HA"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("B"))));
            Assert.IsTrue(String.Equals("[[HELLO, 2]]", ToString(tree.SearchKeyValuePairsOfSimilarKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_29_CheckKeyValuePair_Accessor()
        {
            KeyValuePair<string, int> pair = new KeyValuePair<string, int>("A", 3);
            Assert.IsTrue(String.Equals(pair.Key, "A"));
            Assert.IsTrue(int.Equals(pair.Value, 3));
            String p = pair.ToString();
            Assert.IsTrue(String.Equals("[A, 3]", p));
        }

        [TestMethod]
        public void TEST_30_CheckKeyValuePair_EqualsAndHashCode()
        {
            KeyValuePair<string, int> pair1 = new KeyValuePair<string, int>("A", 1);
            KeyValuePair<string, int> pair2 = new KeyValuePair<string, int>("A", 2);
            KeyValuePair<string, int> pair3 = new KeyValuePair<string, int>("B", 1);

            Assert.IsTrue(pair1.Equals(pair1));
            Assert.IsFalse(pair1.Equals(pair2));
            Assert.IsFalse(pair1.Equals(pair3));
            Assert.IsFalse(pair1.Equals(null));
            Assert.IsFalse(pair1.Equals("A"));
            Assert.IsTrue(pair1.GetHashCode() == pair2.GetHashCode());
            Assert.IsFalse(pair1.GetHashCode() == pair3.GetHashCode());
        }

        [TestMethod]
        public void TEST_31_CheckGetValuesForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);
            tree.Insert("HER", 4);
            tree.Insert("A", 5);

            Assert.IsTrue(String.Equals("[5, 1, 4, 2, 3]", ToString(tree.SearchValuesOfKeysAccordingToPrefix(""))));
            Assert.IsTrue(String.Equals("[5]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("AB"))));
            Assert.IsTrue(String.Equals("[1, 4, 2, 3]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("H"))));
            Assert.IsTrue(String.Equals("[1, 4, 2]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("HE"))));
            Assert.IsTrue(String.Equals("[4, 2]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("HER"))));
            Assert.IsTrue(String.Equals("[3]", ToString(tree.SearchValuesOfKeysAccordingToPrefix("HO"))));
        }

        [TestMethod]
        public void TEST_32_CheckGetKeyValuePairsForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Insert("HELLO", 1);
            tree.Insert("HERE", 2);
            tree.Insert("HOUSE", 3);
            tree.Insert("HER", 4);
            tree.Insert("A", 5);

            Assert.IsTrue(String.Equals("[[A, 5], [HELLO, 1], [HER, 4], [HERE, 2], [HOUSE, 3]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix(""))));
            Assert.IsTrue(String.Equals("[[A, 5]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("AB"))));
            Assert.IsTrue(String.Equals("[[HELLO, 1], [HER, 4], [HERE, 2], [HOUSE, 3]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("H"))));
            Assert.IsTrue(String.Equals("[[HELLO, 1], [HER, 4], [HERE, 2]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("HE"))));
            Assert.IsTrue(String.Equals("[[HER, 4], [HERE, 2]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("HER"))));
            Assert.IsTrue(String.Equals("[[HOUSE, 3]]", ToString(tree.SearchKeyValuePairsAccordingToPrefix("HO"))));
        }

        [TestMethod]
        public void TEST_33_CheckGetKeyValuePairsForPrefix()
        {

        }

            private object ToString(IEnumerable<KeyValuePair<string, int>> enumerable)
        {
            string sb = "[";
            IEnumerator<KeyValuePair<string, int>> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }

        private object ToString(IEnumerable<int> enumerable)
        {
            string sb = "[";
            IEnumerator<int> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }

        private object ToString(IEnumerable<string> enumerable)
        {
            string sb = "[";
            IEnumerator<string> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }
    } 
}
