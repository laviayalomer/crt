﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RadixTree;


namespace RadixTreeTest
{
    [TestClass]
    public class UnitTest
    {
        string nodeSign = ((char)0x0298).ToString();

        [TestMethod]
        public void Test_01_BuildNaiveTree()
        {
            CRT<int> tree = new CRT<int>();
          
            tree.Put("HELLO", 1);
            String staticTreeResult =
                    nodeSign + "\n" +
                    "└── " + nodeSign + " [HELLO, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_02_SetValue()
        {
            CRT<int> tree = new CRT<int>();

            int result;
            result = tree.Put("A", 1);
            Assert.IsTrue(int.Equals(result, 0));
            result = tree.Put("A", 2);
            Assert.IsTrue(int.Equals(result, 1));

            Assert.IsTrue(Equals(2, tree.GetValueForExactKey("A")));
        }

        [TestMethod]
        public void Test_03_CheckNodeSorting()
        {    
            CRT<int> tree = new CRT<int>();
            tree.Put("B", 1);
            tree.Put("A", 2);
            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "├── " + nodeSign + " [A, 2]\n" +
                    "└── " + nodeSign + " [B, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_04_AppendSubstringNode()
        {
            CRT<int> tree = new CRT<int>();
            tree.Put("HE", 1);
            tree.Put("HELLO", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 1]\n" +
                    "    └── " + nodeSign + " [LLO, 2]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_05_CheckNodeSplitting()
        {
            CRT<int> tree = new CRT<int>();
            tree.Put("HELLO", 1);
            tree.Put("HE", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [HE, 2]\n" +
                    "    └── " + nodeSign + " [LLO, 1]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_06_AppendNodeCommonPrefix()
        {
            CRT<int> tree = new CRT<int>();
            tree.Put("HELLO", 1);
            tree.Put("HERE", 2);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " HE\n" + 
                    "    ├── " + nodeSign + " [LLO, 1]\n" +
                    "    └── " + nodeSign + " [RE, 2]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_07_AppendNodeCommonPrefixComplex()
        {
            CRT<int> tree = new CRT<int>();
            tree.Put("HELLO", 1);
            tree.Put("HERE", 2);
            tree.Put("HOUSE", 3);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +             
                    "    ├── " + nodeSign + " E\n" +      
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";
            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }  

        [TestMethod]
        public void Test_08_OverrideValue()
        {
            CRT<int> tree = new CRT<int>();
            int result;

            result = tree.PutIfAbsent("A", 1);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.PutIfAbsent("A", 2);
            Assert.IsTrue(int.Equals(result, 1));

            Assert.IsTrue(Equals(1, tree.GetValueForExactKey("A")));
        }

        [TestMethod]
        public void Test_09_Insert_And_SplitNode()
        {
            CRT<int> tree = new CRT<int>();
            int result;

            result = tree.PutIfAbsent("HELLO", 1);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.PutIfAbsent("HERE", 1);
            Assert.IsTrue(int.Equals(result, 0));

            result = tree.PutIfAbsent("HE", 2);
            Assert.IsTrue(int.Equals(result, 0));
        }

        [TestMethod]
        public void Test_10_keyValidation()
        {
            CRT<int> tree = new CRT<int>();
            try
            {
                tree.Put(null, 1);
            }
            catch(Exception)
            {
                Console.WriteLine("Got the exception staticTreeResult");   
            }
        }

        [TestMethod]
        public void Test_11_valueValidation()
        {
            CRT<int> tree = new CRT<int>();
            try
            {
                tree.Put("A", 0);
            }
            catch (Exception)
            {
                Console.WriteLine("Got the exception staticTreeResult");
            }
        }

        [TestMethod]
        public void Test_12_BuildComplexString()
        {
            CRT<int> tree = new CRT<int>();
            tree.Put("HELLO_MY_FRIEND", 6);
            tree.Put("HELLO_MY", 5);
            tree.Put("HELLO_THERE", 4);
            tree.Put("HELLO_", 3);
            tree.Put("HE", 2);
            tree.Put("H", 1);

            String staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " [H, 1]\n" +
                    "    └── " + nodeSign + " [E, 2]\n" +
                    "        └── " + nodeSign + " [LLO_, 3]\n" +
                    "            ├── " + nodeSign + " [MY, 5]\n" +
                    "            │   └── " + nodeSign + " [_FRIEND, 6]\n" +
                    "            └── " + nodeSign + " [THERE, 4]\n";

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_13_CheckSize()
        {
           CRT<int> tree = new CRT<int>();

           Assert.IsTrue(int.Equals(0, tree.Size()));

           tree.Put("HELLO", 1);
           Assert.IsTrue(int.Equals(1, tree.Size()));

           tree.Put("HERE", 2);
           Assert.IsTrue(int.Equals(2, tree.Size()));

           tree.Put("HOUSE", 3);
           Assert.IsTrue(int.Equals(3, tree.Size()));
       
           tree.Remove("B");
           Assert.IsTrue(int.Equals(3, tree.Size()));

           tree.Remove("HOUSE");
           Assert.IsTrue(int.Equals(2, tree.Size()));

           tree.Remove("HERE");
           Assert.IsTrue(int.Equals(1, tree.Size()));

           tree.Remove("HELLO");
           Assert.IsTrue(int.Equals(0, tree.Size()));
        }

        [TestMethod]
        public void Test_14_CheckGetValueForKey()
        {
           CRT<int> tree = new CRT<int>();

           tree.Put("HELLO", 1);
           tree.Put("HERE", 2);
           tree.Put("HOUSE", 3);

           Assert.IsTrue(int.Equals(1, tree.GetValueForExactKey("HELLO")));
           Assert.IsTrue(int.Equals(2, tree.GetValueForExactKey("HERE")));
           Assert.IsTrue(int.Equals(3, tree.GetValueForExactKey("HOUSE")));
           Assert.IsTrue(int.Equals(0, tree.GetValueForExactKey("H")));
           Assert.IsTrue(int.Equals(0, tree.GetValueForExactKey("HE")));
           Assert.IsTrue(int.Equals(0, tree.GetValueForExactKey("E")));
           Assert.IsTrue(int.Equals(0, tree.GetValueForExactKey("")));
        }

        [TestMethod]
        public void Test_15_Remove_MoreThanOneChildEdge()
        { 
           CRT<int> tree = new CRT<int>();

           tree.Put("H", 1);
           tree.Put("HELLO", 2);
           tree.Put("HAM", 3);

           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [H, 1]\n" +
                   "    ├── " + nodeSign + " [AM, 3]\n" +
                   "    └── " + nodeSign + " [ELLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("H");
           Assert.IsTrue(Removed);

           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " H\n" +
                   "    ├── " + nodeSign + " [AM, 3]\n" +
                   "    └── " + nodeSign + " [ELLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_16_Remove_ExactlyOneChildEdge()
        {
           CRT<int> tree = new CRT<int>();

           tree.Put("H", 1);
           tree.Put("HELLO", 2);
           tree.Put("HELLO_WORLD", 3);
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [H, 1]\n" +
                   "    └── " + nodeSign + " [ELLO, 2]\n" +
                   "        └── " + nodeSign + " [_WORLD, 3]\n";
           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("H");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HELLO, 2]\n" +
                   "    └── " + nodeSign + " [_WORLD, 3]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_17_Remove_ZeroChildEdges_DirectChildOfRoot()
        {
           CRT<int> tree = new CRT<int>();

           tree.Put("A", 1);
           tree.Put("B", 2);
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "├── " + nodeSign + " [A, 1]\n" +
                   "└── " + nodeSign + " [B, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("A");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [B, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_18_Remove_LastRemainingKey()
        {
           CRT<int> tree = new CRT<int>();

           tree.Put("A", 5);
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [A, 5]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("A");
           Assert.IsTrue(Removed);
      
           staticTreeResult =
                   "" + nodeSign + "\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_19_Remove_ZeroChildEdges_OneStepFromRoot()
        { 
           CRT<int> tree = new CRT<int>();

           tree.Put("HE", 1);
           tree.Put("HELLO", 2);  
       
           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n" +
                   "    └── " + nodeSign + " [LLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("HELLO");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void Test_20_Remove_ZeroChildEdges_SeveralStepsFromRoot()
        { 
           CRT<int> tree = new CRT<int>();

           tree.Put("HE", 1);
           tree.Put("HELLO", 2);
           tree.Put("HELLO_WORLD", 3);

           String staticTreeResult, dynamicTreeResult;
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n" +
                   "    └── " + nodeSign + " [LLO, 2]\n" +
                   "        └── " + nodeSign + " [_WORLD, 3]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
       
           bool Removed = tree.Remove("HELLO_WORLD");
           Assert.IsTrue(Removed);
       
           staticTreeResult =
                   "" + nodeSign + "\n" +
                   "└── " + nodeSign + " [HE, 1]\n" +
                   "    └── " + nodeSign + " [LLO, 2]\n";

           dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
           Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_21_testRemove_DoNotRemoveSplitNode()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Put("HELLO", 1);
            radixTree.Put("HERE", 2);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " HE\n" +
                    "    ├── " + nodeSign + " [LLO, 1]\n" +
                    "    └── " + nodeSign + " [RE, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HE");
            Assert.IsFalse(Removed);

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_22_testRemove_MergeSplitNode()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Put("HELLO", 1);
            radixTree.Put("HERE", 2);
            radixTree.Put("HOUSE", 3);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " E\n" +
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HELLO");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [ERE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_23_testRemove_DoNotMergeSplitNodeWithValue()
        {
            CRT<int> radixTree = new CRT<int>();

            radixTree.Put("HELLO", 1);
            radixTree.Put("HERE", 2);
            radixTree.Put("HOUSE", 3);
            radixTree.Put("HE", 4);

            string staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [E, 4]\n" +
                    "    │   ├── " + nodeSign + " [LLO, 1]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = radixTree.Remove("HELLO");
            Assert.IsTrue(Removed);

            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "└── " + nodeSign + " H\n" +
                    "    ├── " + nodeSign + " [E, 4]\n" +
                    "    │   └── " + nodeSign + " [RE, 2]\n" +
                    "    └── " + nodeSign + " [OUSE, 3]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(radixTree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        public void TEST_24_testRemove_NoSuchKey()
        {
            CRT<int> tree = new CRT<int>();

            tree.Put("A", 1);
            tree.Put("B", 2);

            String staticTreeResult, dynamicTreeResult;
            staticTreeResult =
                    "" + nodeSign + "\n" +
                    "├── " + nodeSign + " [A, 1]\n" +
                    "└── " + nodeSign + " [B, 2]\n";

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));

            bool Removed = tree.Remove("HELLO");
            Assert.IsFalse(Removed);

            dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);
            Assert.IsTrue(String.Equals(staticTreeResult, dynamicTreeResult));
        }

        [TestMethod]
        public void TEST_25_testGetKeysForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Put("HELLO", 1);
            tree.Put("HERE", 2);
            tree.Put("HOUSE", 3);
            tree.Put("HER", 4);
            tree.Put("A", 5);

            Assert.IsTrue(String.Equals("[A, HELLO, HER, HERE, HOUSE]", ToString(tree.GetKeysStartingWith(""))));
            Assert.IsTrue(String.Equals("[A]", ToString(tree.GetKeysStartingWith("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.GetKeysStartingWith("AB"))));
            Assert.IsTrue(String.Equals("[HELLO, HER, HERE, HOUSE]", ToString(tree.GetKeysStartingWith("H"))));
            Assert.IsTrue(String.Equals("[HELLO, HER, HERE]", ToString(tree.GetKeysStartingWith("HE"))));
            Assert.IsTrue(String.Equals("[HER, HERE]", ToString(tree.GetKeysStartingWith("HER"))));
            Assert.IsTrue(String.Equals("[HOUSE]", ToString(tree.GetKeysStartingWith("HO"))));
        }

        [TestMethod]
        public void TEST_26_CheckGetClosestKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Put("HE", 1);
            tree.Put("HELLO", 2);
            tree.Put("HAM", 3);

            Assert.IsTrue(String.Equals("[HAM, HE, HELLO]", ToString(tree.GetClosestKeys("HI"))));
            Assert.IsTrue(String.Equals("[HE, HELLO]", ToString(tree.GetClosestKeys("HE"))));
            Assert.IsTrue(String.Equals("[HELLO]", ToString(tree.GetClosestKeys("HELL"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.GetClosestKeys("B"))));
            Assert.IsTrue(String.Equals("[HELLO]", ToString(tree.GetClosestKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_27_CheckGetValuesForClosestKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Put("HE", 1);
            tree.Put("HELLO", 2);
            tree.Put("HAM", 3);

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);

            Assert.IsTrue(String.Equals("[3, 1, 2]", ToString(tree.GetValuesForClosestKeys("HI"))));
            Assert.IsTrue(String.Equals("[1, 2]", ToString(tree.GetValuesForClosestKeys("HE"))));
            Assert.IsTrue(String.Equals("[3]", ToString(tree.GetValuesForClosestKeys("HA"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.GetValuesForClosestKeys("B"))));
            Assert.IsTrue(String.Equals("[2]", ToString(tree.GetValuesForClosestKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_28_CheckGetKeyValuePairsForClosestKeys()
        {
            CRT<int> tree = new CRT<int>();

            tree.Put("HE", 1);
            tree.Put("HELLO", 2);
            tree.Put("HAM", 3);

            String dynamicTreeResult = MyStringBuilder<int>.BuildStringFromTree(tree);

            Assert.IsTrue(String.Equals("[[HAM, 3], [HE, 1], [HELLO, 2]]", ToString(tree.GetKeyValuePairsForClosestKeys("HI"))));
            Assert.IsTrue(String.Equals("[[HE, 1], [HELLO, 2]]", ToString(tree.GetKeyValuePairsForClosestKeys("HE"))));
            Assert.IsTrue(String.Equals("[[HAM, 3]]", ToString(tree.GetKeyValuePairsForClosestKeys("HA"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.GetKeyValuePairsForClosestKeys("B"))));
            Assert.IsTrue(String.Equals("[[HELLO, 2]]", ToString(tree.GetKeyValuePairsForClosestKeys("HELLO_WORLD"))));
        }

        [TestMethod]
        public void TEST_29_CheckKeyValuePair_Accessor()
        {
            KeyValuePair<string, int> pair = new KeyValuePair<string, int>("A", 3);
            Assert.IsTrue(String.Equals(pair.Key, "A"));
            Assert.IsTrue(int.Equals(pair.Value, 3));
            String p = pair.ToString();
            Assert.IsTrue(String.Equals("[A, 3]", p));
        }

        [TestMethod]
        public void TEST_30_CheckKeyValuePair_EqualsAndHashCode()
        {
            KeyValuePair<string, int> pair1 = new KeyValuePair<string, int>("A", 1);
            KeyValuePair<string, int> pair2 = new KeyValuePair<string, int>("A", 2);
            KeyValuePair<string, int> pair3 = new KeyValuePair<string, int>("B", 1);

            Assert.IsTrue(pair1.Equals(pair1));
            Assert.IsFalse(pair1.Equals(pair2));
            Assert.IsFalse(pair1.Equals(pair3));
            Assert.IsFalse(pair1.Equals(null));
            Assert.IsFalse(pair1.Equals("A"));
            Assert.IsTrue(pair1.GetHashCode() == pair2.GetHashCode());
            Assert.IsFalse(pair1.GetHashCode() == pair3.GetHashCode());
        }

        [TestMethod]
        public void TEST_31_CheckGetValuesForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Put("HELLO", 1);
            tree.Put("HERE", 2);
            tree.Put("HOUSE", 3);
            tree.Put("HER", 4);
            tree.Put("A", 5);

            Assert.IsTrue(String.Equals("[5, 1, 4, 2, 3]", ToString(tree.GetValuesForKeysStartingWith(""))));
            Assert.IsTrue(String.Equals("[5]", ToString(tree.GetValuesForKeysStartingWith("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.GetValuesForKeysStartingWith("AB"))));
            Assert.IsTrue(String.Equals("[1, 4, 2, 3]", ToString(tree.GetValuesForKeysStartingWith("H"))));
            Assert.IsTrue(String.Equals("[1, 4, 2]", ToString(tree.GetValuesForKeysStartingWith("HE"))));
            Assert.IsTrue(String.Equals("[4, 2]", ToString(tree.GetValuesForKeysStartingWith("HER"))));
            Assert.IsTrue(String.Equals("[3]", ToString(tree.GetValuesForKeysStartingWith("HO"))));
        }

        [TestMethod]
        public void TEST_32_CheckGetKeyValuePairsForPrefix()
        {
            CRT<int> tree = new CRT<int>();

            tree.Put("HELLO", 1);
            tree.Put("HERE", 2);
            tree.Put("HOUSE", 3);
            tree.Put("HER", 4);
            tree.Put("A", 5);

            Assert.IsTrue(String.Equals("[[A, 5], [HELLO, 1], [HER, 4], [HERE, 2], [HOUSE, 3]]", ToString(tree.GetKeyValuePairsForKeysStartingWith(""))));
            Assert.IsTrue(String.Equals("[[A, 5]]", ToString(tree.GetKeyValuePairsForKeysStartingWith("A"))));
            Assert.IsTrue(String.Equals("[]", ToString(tree.GetKeyValuePairsForKeysStartingWith("AB"))));
            Assert.IsTrue(String.Equals("[[HELLO, 1], [HER, 4], [HERE, 2], [HOUSE, 3]]", ToString(tree.GetKeyValuePairsForKeysStartingWith("H"))));
            Assert.IsTrue(String.Equals("[[HELLO, 1], [HER, 4], [HERE, 2]]", ToString(tree.GetKeyValuePairsForKeysStartingWith("HE"))));
            Assert.IsTrue(String.Equals("[[HER, 4], [HERE, 2]]", ToString(tree.GetKeyValuePairsForKeysStartingWith("HER"))));
            Assert.IsTrue(String.Equals("[[HOUSE, 3]]", ToString(tree.GetKeyValuePairsForKeysStartingWith("HO"))));
        }

        [TestMethod]
        public void TEST_33_CheckGetKeyValuePairsForPrefix()
        {

        }

            private object ToString(IEnumerable<KeyValuePair<string, int>> enumerable)
        {
            string sb = "[";
            IEnumerator<KeyValuePair<string, int>> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }

        private object ToString(IEnumerable<int> enumerable)
        {
            string sb = "[";
            IEnumerator<int> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }

        private object ToString(IEnumerable<string> enumerable)
        {
            string sb = "[";
            IEnumerator<string> iterable = enumerable.GetEnumerator();
            while (iterable.MoveNext())
            {
                sb += (iterable.Current) + ", ";
            }
            sb = sb.TrimEnd(' ');
            sb = sb.TrimEnd(',');
            sb += "]";
            return sb.ToString();
        }
    } 
}
