﻿using System;
using System.Collections.Generic;
using System.Text;
using static RadixTree.InnerExceptions;

namespace RadixTree
{
    public class Node<T>
    {
        public Node(T value, string InEdges, List<Node<T>> OutEdges, bool isRoot = false)
        {
            //Checks the Node arguments
            CheckNodeArguments(InEdges, OutEdges, isRoot);

            CheckForDuplicateEdges(OutEdges); 
            if(OutEdges.Size() == 0) { LeafNode_Config(value, InEdges); }
            else { InnerNode_Config(value, InEdges, OutEdges); }
        }

        /// <summary>
        /// Checks the validity of the arguments
        /// </summary>
        /// <param name="InEdges"></param>
        /// <param name="childNodes"></param>
        /// <param name="isRoot"></param>
        private void CheckNodeArguments(string InEdges, List<Node<T>> childNodes, bool isRoot)
        {
            if (InEdges == null)
            {
                throw new InvalidEdgesException();
            }
            if (!isRoot && InEdges.Length == 0)
            {
                throw new InvalidEdgesException();
            }
            if (childNodes == null)
            {
                throw new ArgumentNullException("The childNodes argument was null");
            }
        }

        /// <summary>
        /// Config the node as a inner node & initialize the OutEdges & OutEdgesList members
        /// </summary>
        /// <param name="value"></param>
        /// <param name="InEdges"></param>
        /// <param name="OutEdges"></param>
        private void InnerNode_Config(T value, string InEdges, List<Node<T>> OutEdges)
        {
            Node<T>[] OutEdgesArray = OutEdges.ListToArray();
            Array.Sort(OutEdgesArray, new NodeComparator());
            this.OutEdges = new ConcurrentArray<Node<T>>(OutEdgesArray);
            this.InEdges = InEdges;
            this.value = value;
            OutEdgesList = new ConcurrentArrayAsList<Node<T>>(this.OutEdges);
        }

        /// <summary>
        /// Config the node as a leaf node 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="InEdges"></param>
        private void LeafNode_Config(T value, string InEdges)
        {
            OutEdgesList = new List<Node<T>>();
            this.InEdges = InEdges;
            this.value = value;
        }

        //====>> Getters <<====//

        /// <summary>
        /// Gets the In Edges
        /// </summary>
        /// <returns></returns>
        public string GetInEdges()
        {
            return this.InEdges;
        }

        /// <summary>
        /// Gets the first char in the In edges
        /// </summary>
        /// <returns></returns>
        public char GetFirstInEdges()
        {
            return this.InEdges[0];
        }

        public T GetValue()
        {
            return value;
        }

        /// <summary>
        /// Gets the OutEdges as list of nodes
        /// </summary>
        /// <returns></returns>
        public List<Node<T>> GetOutEdgesList()
        {
            return OutEdgesList;
        }

        /// <summary>
        /// Gets the node of the OutEdges by a given char
        /// </summary>
        /// <param name="edgeFirstCharacter"></param>
        /// <returns></returns>
        public Node<T> GetOutEdgeByFirstChar(char edgeFirstCharacter)
        {
            int index;
            //Indicates inner Node
            if (OutEdgesList.Size() != 0 && (index = BinarySearchForEdge(OutEdges, edgeFirstCharacter)) >= 0)    
            {
                return OutEdges.Get(index);
            }
            return null;
        }
        
        /// <summary>
        /// Updates the OutEdges of the given node
        /// </summary>
        /// <param name="childNode"></param>
        public void UpdateOutEdgeByChildNode(Node<T> node)
        {
            int index;
            //Indicates inner Node
            if (OutEdgesList.Size() != 0 && (index = BinarySearchForEdge(OutEdges, node.GetFirstInEdges())) >= 0)
            {
                OutEdges.ConcurrentSet(index, node);
                return;
            }
            throw new Exception("Cannot update the reference to the following child node for the edge starting with '" + node.GetFirstInEdges() + "', no such edge already exists: " + node);
        }
        
        override public string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Node =[Value =");

            if (!value.Equals(default(T))) { stringBuilder.Append(value); }
            else { stringBuilder.Append("null"); }

            stringBuilder.Append(", InEdges = "+ (InEdges.Length==0?"[]": InEdges));

            if(OutEdgesList.Size() == 0) { stringBuilder.Append(", OutEdges=[]"); }
            else { stringBuilder.Append(", OutEdges=" + OutEdgesList.ToString()); }

            return stringBuilder.ToString();
        }

        //====>> Helper Methods <<====//

        /// <summary>
        /// Search for the index of the given 'FirstCharOfEdge' by a binary search
        /// </summary>
        /// <param name="OutEdges"></param>
        /// <param name="FirstCharOfEdge"></param>
        /// <returns></returns>
        private int BinarySearchForEdge(ConcurrentArray<Node<T>> OutEdges, char FirstCharOfEdge)
        {
            int low = 0;
            int high = OutEdges.size - 1;

            while (low <= high)
            {
                int middle = (low + high) / 2;
                int CompareRes = OutEdges.Get(middle).GetFirstInEdges().CompareTo(FirstCharOfEdge);

                if (CompareRes == 0)
                {
                    return middle;
                }
                else if (CompareRes < 0)
                {
                    low = middle + 1;
                }
                else
                {
                    high = middle - 1;
                }
            }
            return -1;  // key not found
        }

        /// <summary>
        /// Verify that the node list has no duplications on its edges
        /// </summary>
        /// <param name="nodeList"></param>
        private void CheckForDuplicateEdges(List<Node<T>> nodeList)
        {
            ICollection<char> distinctChars = new HashSet<char>();
            for (int i = 0; i < nodeList.Size(); i++)
            {
                distinctChars.Add(nodeList.GetIndex(i).GetFirstInEdges());
            }
            if (nodeList.Size() != distinctChars.Count)
            {
                throw new DuplicateEdgeDetected("Duplicate edge detected in list of nodes supplied: " + nodeList);
            }
        }

        //====>> Helper Class for comparsion of Node<T> <<====//

        private class NodeComparator : IComparer<Node<T>>
        {
            int IComparer<Node<T>>.Compare(Node<T> x, Node<T> y)
            {
                return x.GetFirstInEdges().CompareTo(y.GetFirstInEdges());
            }
        }

        private string InEdges;
        private ConcurrentArray<Node<T>> OutEdges;
        private List<Node<T>> OutEdgesList;
        private T value;
    }
}
