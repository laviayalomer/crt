﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ChartsForBenchMarks
{
    public partial class ChartsForBenchMarks : Form
    {
        public string pathDir = @"Results";
        public Series s2 = new Series();
        public Series s3 = new Series();
        public List<string> threadsMap = new List<string> { "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "65" };


        public ChartsForBenchMarks()
        {
            InitializeComponent();
            
            foreach(string th in threadsMap)
            {
                comboBox1.Items.Add(th);
            }
            
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            
        }

        public class MeasurementsJson
        {
            public int NumberOfKeys { get; set; }
            public int PercantageRemoval { get; set; }
            public int PercantageInserts { get; set; }
            public int NumberOfInserts { get; set; }
            public int NumberOfRemovals { get; set; }
            public int TreeDepth { get; set; }

            public int NumberOfThreads { get; set; }

            public int IsBulk { get; set; }
            public int NonConcurrentMeasured { get; set; }

            public string NonConcurrentResult { get; set; }
            public string CourseGrainedLockResult { get; set; }
            public string FineGrainedLockResult
            {
                get; set;
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            CreateSerieses();
        }

        private void CreateSerieses()
        {
            ClearAndInit();
            string threadNum = comboBox1.SelectedItem.ToString();
            int index = threadsMap.IndexOf(threadNum);

            DirectoryInfo dir = new DirectoryInfo(pathDir + @"\" + (checkBox1.Checked ? @"Bulk\" : @"NonBulk\"));

            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                using (var reader = new StreamReader(file.FullName))
                {
                    var responseBody = reader.ReadToEnd();
                    MeasurementsJson[] measures = js.Deserialize<MeasurementsJson[]>(responseBody);
                    double milliCG = TimeSpan.Parse(measures[index].CourseGrainedLockResult).TotalMilliseconds;
                    double milliFG = TimeSpan.Parse(measures[index].FineGrainedLockResult).TotalMilliseconds;
                    double keys = measures[index].NumberOfKeys;
                    dataGridView1.Rows.Add(measures[0].NumberOfKeys, measures[0].TreeDepth);
                    s2.Points.AddXY(keys, milliCG);
                    s3.Points.AddXY(keys, milliFG);
                }
            }
            s2.Sort(PointSortOrder.Ascending);
            s3.Sort(PointSortOrder.Ascending);

            chart1.ChartAreas[0].AxisX.Minimum = 0;
            if (checkBox1.Checked)
            {
                chart1.ChartAreas[0].AxisX.Maximum = 3200000;
                chart1.ChartAreas[0].AxisX.Interval = 400000;
            }
            else
            {
                chart1.ChartAreas[0].AxisX.Maximum = 510000;
                chart1.ChartAreas[0].AxisX.Interval = 100000;
            }

            chart1.ChartAreas[0].AxisX.IntervalOffsetType = DateTimeIntervalType.Number;
            chart1.Series.Add(s2);
            chart1.Series.Add(s3);
            dataGridView1.Sort(dataGridView1.Columns[0], System.ComponentModel.ListSortDirection.Ascending);

            Refresh();
        }

        private void ClearAndInit()
        {
            dataGridView1.Rows.Clear();
            s2.Points.Clear();
            s3.Points.Clear();
            chart1.Series.Clear();
            chart1.ChartAreas.Clear();
            ChartArea ca = new ChartArea();
            chart1.ChartAreas.Add(ca);
            chart1.ChartAreas[0].AxisX.Title = "Number of Nodes";
            chart1.ChartAreas[0].AxisY.Title = "Time [ms]";
            s2.Name = "Coarse Grained";
            s3.Name = "Fine Grained";
        }

        public void CheckChartType()
        {
            switch (comboBox2.SelectedItem)
            {
                case "Line":
                    s3.ChartType = SeriesChartType.Line;
                    s2.ChartType = SeriesChartType.Line;
                    break;
                case "FastLine":
                    s3.ChartType = SeriesChartType.FastLine;
                    s2.ChartType = SeriesChartType.FastLine;
                    break;
                case "Column":
                    s3.ChartType = SeriesChartType.Column;
                    s2.ChartType = SeriesChartType.Column;
                    break;
                case "Point":
                    s3.ChartType = SeriesChartType.Point;
                    s2.ChartType = SeriesChartType.Point;
                    break;
                case "PointAndFigure":
                    s3.ChartType = SeriesChartType.PointAndFigure;
                    s2.ChartType = SeriesChartType.PointAndFigure;
                    break;
            }
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            CheckChartType();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CreateSerieses();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string bulk = pathDir + @"\Bulk\";
            Form2 form = new Form2(bulk);
            form.Show();
        }
    }
}
