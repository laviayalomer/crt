﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixTree_FineGrained
{
    class InnerExceptions
    {
        public class KeyIsNullException : Exception
        {
            public KeyIsNullException() : this("The key passed was null.")
            {
            }

            public KeyIsNullException(string message)
                : base(message)
            {
            }

            public KeyIsNullException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        public class KeyInsertedOfZeroLengthException : Exception
        {
            public KeyInsertedOfZeroLengthException() : this("Can't insert keys of length 0 into the tree.")
            {
            }

            public KeyInsertedOfZeroLengthException(string message)
                : base(message)
            {
            }

            public KeyInsertedOfZeroLengthException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        public class ValueInsertedhDefaultException : Exception
        {
            public ValueInsertedhDefaultException() : this("Invalid value. Can't accept this value for tree of this type.")
            {
            }

            public ValueInsertedhDefaultException(string message)
                : base(message)
            {
            }

            public ValueInsertedhDefaultException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        public class InvalidEdgesException : Exception
        {
            public InvalidEdgesException() : this("Invalid edges. null or empty.")
            {
            }

            public InvalidEdgesException(string message)
                : base(message)
            {
            }

            public InvalidEdgesException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        public class UnexpectedOperationForSearchEntityType : Exception
        {
            public UnexpectedOperationForSearchEntityType() : this("Unexpected operation for this type of search entity")
            {
            }

            public UnexpectedOperationForSearchEntityType(string message)
                : base(message)
            {
            }

            public UnexpectedOperationForSearchEntityType(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        public class UnableToExtractSearchEntityType : Exception
        {
            public UnableToExtractSearchEntityType() : this("Something went wrong. unable to extract SearchEntity type")
            {
            }

            public UnableToExtractSearchEntityType(string message)
                : base(message)
            {
            }

            public UnableToExtractSearchEntityType(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        public class DuplicateEdgeDetected : Exception
        {
            public DuplicateEdgeDetected() : this("Duplicate edge detected")
            {
            }

            public DuplicateEdgeDetected(string message)
                : base(message)
            {
            }

            public DuplicateEdgeDetected(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

    }
}
