﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace RadixTree_CoarseGrained
{
    /* This class represents all Enumerators we will use to maintain correctness
     * of the tree. O represents the generic type of a node's  value. */
    public class Enumerators<T>
    {

        /* Given a string str IRTSuffixEnumerable will return an Iterator
         * To iterate through the different suffixes of str */

        public abstract class BaseEnumerator<O> : IEnumerator<O>
        {
            object IEnumerator.Current
            {
                get
                {
                    return CurrentGeneric;
                }
            }

            O IEnumerator<O>.Current
            {
                get
                {
                    return CurrentGeneric;
                }
            }

            public O CurrentGeneric
            {
                get;
                set;
            }
            public abstract bool MoveNext();

            public abstract void Reset();

            public void Dispose()
            {
                throw new NotImplementedException();
            }
        }

        public class BaseValuesEnumerable : IEnumerable<T>
        {
            private Node<T> head;
            private string key;

            public BaseValuesEnumerable(Node<T> head, string key)
            {
                this.head = head;
                this.key = key;
            }

            public IEnumerator GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                return new BaseValuesEnumerator(head, key);
            }
        }

        public class BaseValuesEnumerator : BaseEnumerator<T>
        {
            BaseEnumerator<NodeKeyPair<T>> childrenNodeKeyPair;

            public BaseValuesEnumerator(Node<T> head, string key)
            {
                childrenNodeKeyPair = (BaseEnumerator<NodeKeyPair<T>>)(new BaseTreeTraverseEnumerable(head, key)).GetEnumerator();
            }

            public override bool MoveNext()
            {
                while (childrenNodeKeyPair.MoveNext())
                {
                    NodeKeyPair<T> nodeKeyPair = childrenNodeKeyPair.CurrentGeneric;
                    T value = (T)nodeKeyPair.node.GetValue();
                    if (!value.Equals(default(T)))
                    {
                        CurrentGeneric = value;
                        return true;
                    }
                }
                
                CurrentGeneric = default(T);
                return false;
            }

            public override void Reset()
            {
                throw new NotImplementedException();
            }
        }

        public class EmptyBaseEnumerable<O> : IEnumerable<O>
        {
            public EmptyBaseEnumerable()
            {
            }

            public IEnumerator GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator<O> IEnumerable<O>.GetEnumerator()
            {
                return new EmptyBaseEnumerator<O>();
            }
        }

        public class EmptyBaseEnumerator<O> : BaseEnumerator<O>
        {
            public EmptyBaseEnumerator()
            {
            }

            public override bool MoveNext()
            {
                return false;
            }

            public override void Reset()
            {
                throw new NotImplementedException();
            }
        }

        public class BaseKeysEnumerable<O> : IEnumerable<O>
        {
            private Node<T> head;
            private string key;

            public BaseKeysEnumerable(Node<T> head, string key)
            {
                this.head = head;
                this.key = key;
            }

            public IEnumerator GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator<O> IEnumerable<O>.GetEnumerator()
            {
                return (IEnumerator<O>)new BaseKeysEnumerator<O>(head, key);
            }
        }

        public class BaseKeysEnumerator<O> : BaseEnumerator<string>
        {
            BaseEnumerator<NodeKeyPair<T>> childrenNodeKeyPair;

            public BaseKeysEnumerator(Node<T> head, string key)
            {
                childrenNodeKeyPair = (BaseEnumerator<NodeKeyPair<T>>)(new BaseTreeTraverseEnumerable(head, key)).GetEnumerator();
            }

            public override void Reset()
            {
                throw new NotImplementedException();
            }
            public override bool MoveNext()
            {
                while (childrenNodeKeyPair.MoveNext())
                {
                    NodeKeyPair<T> nodeKeyPair = childrenNodeKeyPair.CurrentGeneric;
                    T value = nodeKeyPair.node.GetValue();
                    if (!value.Equals(default(T)))
                    {
                        CurrentGeneric = nodeKeyPair.key;
                        return true;
                    }
                }

                CurrentGeneric = null;
                return false;
            }
        }

        public class BaseKeyValuePairsEnumerable : IEnumerable<KeyValuePair<string, T>>
        {
            private Node<T> head;
            private string key;

            public BaseKeyValuePairsEnumerable(Node<T> head, string key)
            {
                this.head = head;
                this.key = key;
            }

            public IEnumerator GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator<KeyValuePair<string, T>> IEnumerable<KeyValuePair<string, T>>.GetEnumerator()
            {
                return new BaseKeyValuePairsEnumerator<KeyValuePair<string, T>>(head, key);
            }
        }

        public class BaseKeyValuePairsEnumerator<O> : BaseEnumerator<KeyValuePair<string, T>>
        {
            BaseEnumerator<NodeKeyPair<T>> childrenNodeKeyPair;

            public BaseKeyValuePairsEnumerator(Node<T> head, string key)
            {
                childrenNodeKeyPair = (BaseEnumerator<NodeKeyPair<T>>)(new BaseTreeTraverseEnumerable(head, key)).GetEnumerator();
            }

            public override void Reset()
            {
                throw new NotImplementedException();
            }

            public override bool MoveNext()
            {
                while (childrenNodeKeyPair.MoveNext())
                {
                    NodeKeyPair<T> nodeKeyPair = childrenNodeKeyPair.CurrentGeneric;
                    T value = (T)nodeKeyPair.node.GetValue();
                    if (!value.Equals(default(T)))
                    {                        
                        string keyString = nodeKeyPair.key;
                        CurrentGeneric = new KeyValuePair<string, T>(keyString, value);
                        return true;
                    }
                }

                CurrentGeneric = default(KeyValuePair<string, T>);
                return false;
            }
        }

        public class BaseTreeTraverseEnumerable : IEnumerable<NodeKeyPair<T>>
        {
            Node<T> head;
            string key;
            public BaseTreeTraverseEnumerable(Node<T> head, string key)
            {
                this.head = head;
                this.key = key;
            }

            public IEnumerator GetEnumerator()
            {
                return new BaseTreeTraverseEnumerator(head, key);
            }

            IEnumerator<NodeKeyPair<T>> IEnumerable<NodeKeyPair<T>>.GetEnumerator()
            {
                return new BaseTreeTraverseEnumerator(head, key);
            }
        }

        public class BaseTreeTraverseEnumerator : BaseEnumerator<NodeKeyPair<T>>
        {
            LinkedList<NodeKeyPair<T>> stack;

            public BaseTreeTraverseEnumerator(Node<T> head, string key)
            {
                stack = new LinkedList<NodeKeyPair<T>>();
                stack.AddFirst(new NodeKeyPair<T>(head, key));
            }

            public override void Reset()
            {
                throw new NotImplementedException();
            }

            public override bool MoveNext()
            {

                if (stack.Count == 0)
                {
                    CurrentGeneric = null;
                    return false;
                }

                CurrentGeneric = stack.First();
                stack.RemoveFirst();
                List<Node<T>> childNodes = CurrentGeneric.node.GetOutGoingEdgesList();
                
                for (int i = childNodes.Size(); i > 0; i--)
                {
                    Node<T> child = childNodes.GetIndex(i - 1);
                    stack.AddFirst(new NodeKeyPair<T>(child, RadixTreeUtils<T>.Concatenate(CurrentGeneric.key, child.GetIncomingEdges()).ToString()));
                }

                return true;
            }
        }
    }
}