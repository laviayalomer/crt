﻿using System;
using System.Collections.Generic;
using System.Linq;
using static RadixTree.InnerExceptions;

namespace RadixTree
{
    public class CRT<T>
    {
        private static readonly Object obj = new Object();

        public volatile Node<T> root;

        public SearchEntities_Factory<T> searchEntityFactory;

        public CRT()
        {
            root = new Node<T>(default(T), "", new List<Node<T>>(), true);
            searchEntityFactory = new SearchEntities_Factory<T>(this);
        }

        public IEnumerable<T> GetDescendantValues(string startKey, Node<T> startNode)
        {
            return new Enumerators<T>.BaseValuesEnumerable(startNode, startKey);
        }

        public T Put(string key, T value)
        {
            if (value.Equals(default(T)))
            {
                throw new ValueInsertedhDefaultException();
            }

            return PutInternal(key, value, true);
        }

        public T PutIfAbsent(string key, T value)
        {
            return PutInternal(key, value, false);
        }

        public T GetValueForExactKey(string key)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
            return result.GetValueForKeyFound();
        }

        public IEnumerable<string> GetKeysStartingWith(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetKeysWithGivenPrefix();
        }

        public IEnumerable<T> GetValuesForKeysStartingWith(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetValuesOfKeysWithGivenPrefix();
        }

        public IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsForKeysStartingWith(string prefix)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(prefix);
            return result.GetKeyValuePairsOfKeysWithGivenPrefix();
        }

        public bool Remove(string key)
        {
            if (key == null)
            {
                throw new KeyIsNullException();
            }
            lock (obj)
            {
                try
                {
                    SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                    return result.Remove();
                }
                catch (Exception) { Console.WriteLine("Exception during search entity creation"); }
            }
            return false;
        }

        public IEnumerable<string> GetClosestKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetKeysWithLongestGivenPrefix();
        }

        public IEnumerable<T> GetValuesForClosestKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetValuesOfKeysWithLongestGivenPrefix();
        }

        public IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsForClosestKeys(string candidate)
        {
            SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(candidate);
            return result.GetKeyValuePairsOfKeysWithLongestGivenPrefix();
        }

        public int Size()
        {
            LinkedList<Node<T>> stack = new LinkedList<Node<T>>();
            stack.AddFirst(root);
            int count = 0;
            while (true)
            {
                if (stack.Count == 0)
                {
                    return count;
                }
                Node<T> current = stack.First();
                stack.RemoveFirst();
                stack.AddAll(current.GetOutEdgesList());
                if (!current.GetValue().Equals(default(T)))
                {
                    count++;
                }
            }
        }

        // ------------- Helper method for put() -------------

        /**
         * Atomically adds the given value to the tree, creating a node for the value as necessary. If the value is already
         * stored for the same key, either overwrites the existing value, or simply returns the existing value, depending
         * on the given value of the <code>overwrite</code> flag.
         *
         * @param key The key against which the value should be stored
         * @param value The value to store against the key
         * @param overwrite If true, should replace any existing value, if false should not replace any existing value
         * @return The existing value for this key, if there was one, otherwise null
         */
        T PutInternal(string key, T value, bool overwrite)
        {
            if (key == null)
            {
                throw new KeyIsNullException();
            }
            if (key.Length == 0)
            {
                throw new KeyInsertedOfZeroLengthException();
            }
            if (value.Equals(default(T)))
            {
                throw new ValueInsertedhDefaultException();
            }
            lock (obj)
            {
                try
                {
                    SearchEntities<T> result = searchEntityFactory.CreateSearchEntity(key);
                    return result.Insert(overwrite, value);
                }
                catch (Exception)
                {
                    Console.WriteLine("Breakpoint");
                }
            }

            return default(T);
        }



        // ------------- Helper method for finding descendants of a given node -------------

        /**
         * Returns a lazy iterable which will return {@link string} keys for which the given key is a prefix.
         * The results inherently will not contain duplicates (duplicate keys cannot exist in the tree).
         * <p/>
         * Note that this method internally converts {@link string}s to {@link String}s, to avoid set equality issues,
         * because equals() and hashCode() are not specified by the string API contract.
*/
        public IEnumerable<string> GetDescendantKeys(string startKey, Node<T> startNode)
        {
            return (IEnumerable<string>)(new Enumerators<T>.BaseKeysEnumerable<string>(startNode, startKey));
        }

        /**
         * Returns a lazy iterable which will return values which are associated with keys in the tree for which
         * the given key is a prefix.
         */


        /**
         * Returns a lazy iterable which will return {@link KeyValuePair} objects each containing a key and a value,
         * for which the given key is a prefix of the key in the {@link KeyValuePair}. These results inherently will not
         * contain duplicates (duplicate keys cannot exist in the tree).
         * <p/>
         * Note that this method internally converts {@link string}s to {@link String}s, to avoid set equality issues,
         * because equals() and hashCode() are not specified by the string API contract.
         */
        public IEnumerable<KeyValuePair<string, T>> GetDescendantKeyValuePairs(string startKey, Node<T> startNode)
        {
            return (IEnumerable<KeyValuePair<string, T>>)new Enumerators<T>.BaseKeyValuePairsEnumerable(startNode, startKey);
        }


        /**
         * Traverses the tree using depth-first, preordered traversal, starting at the given node, using lazy evaluation
         * such that the next node is only determined when next() is called on the iterator returned.
         * The traversal algorithm uses iteration instead of recursion to allow deep trees to be traversed without
         * requiring large JVM stack sizes.
         * <p/>
         * Each node that is encountered is returned from the iterator along with a key associated with that node,
         * in a NodeKeyPair<T> object. The key will be prefixed by the given start key, and will be generated by appending
         * to the start key the edges traversed along the path to that node from the start node.
         *
         * @param startKey The key which matches the given start node
         * @param startNode The start node
         * @return An iterator which when iterated traverses the tree using depth-first, preordered traversal,
         * starting at the given start node
         */
        public IEnumerable<NodeKeyPair<T>> LazyTraverseDescendants(string startKey, Node<T> startNode)
        {
            return new Enumerators<T>.BaseTreeTraverseEnumerable(startNode, startKey);
        }
    }
}