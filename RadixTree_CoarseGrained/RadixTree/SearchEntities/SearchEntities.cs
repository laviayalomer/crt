﻿
using System.Collections.Generic;

namespace RadixTree
{
    public abstract class SearchEntities<T>
    {
        public string key;
        public readonly Node<T> resultNode;
        public readonly int totalNumOfEqualChars;
        public readonly int numOfEqualNodeChars;
        public readonly Node<T> parent;
        public readonly Node<T> grandParent;
        public CRT<T> currentTree;

        public SearchEntities(SearchEntities_Factory<T>.InnerSearchResult res)
        {
            key = res.Key;
            resultNode = res.ResultNode;
            totalNumOfEqualChars = res.TotalNumOfEqualChars;
            numOfEqualNodeChars = res.NumOfEqualNodeChars;
            parent = res.Parent;
            grandParent = res.GrandParent;
            currentTree = res.CurrentTree;
        }

        public abstract T GetValueForKeyFound();

        public abstract IEnumerable<string> GetKeysWithGivenPrefix();

        public abstract IEnumerable<T> GetValuesOfKeysWithGivenPrefix();

        public abstract IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix();

        public abstract IEnumerable<string> GetKeysWithLongestGivenPrefix();

        public abstract IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix();

        public abstract IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix();

        public abstract T Insert(bool overwrite, T value);

        public abstract bool Remove();

    }
}
