﻿
using System.Collections.Generic;
using System.Threading;

namespace RadixTree
{
    public abstract class SearchEntities<T>
    {
        public string key;
        public readonly Node<T> resultNode;
        public readonly int totalNumOfEqualChars;
        public readonly int numOfEqualNodeChars;
        public readonly Node<T> parent;
        public readonly Node<T> grandParent;
        public CRT<T> currentTree;

        public enum ValidationState
        {
            SUCCESS,
            FAILURE,
            KEEP_GOING
        }

        public SearchEntities(SearchEntities_Factory<T>.InnerSearchResult res)
        {
            key = res.Key;
            resultNode = res.ResultNode;
            totalNumOfEqualChars = res.TotalNumOfEqualChars;
            numOfEqualNodeChars = res.NumOfEqualNodeChars;
            parent = res.Parent;
            grandParent = res.GrandParent;
            currentTree = res.CurrentTree;
        }


        protected ValidationState ValidateInsert()
        {
            SearchEntities<T> validationResult = currentTree.searchEntityFactory.CreateSearchEntity(key);
            if (validationResult.resultNode != resultNode || validationResult.parent != parent || validationResult.grandParent != grandParent )
            {
                return ValidationState.KEEP_GOING;
            }

            return ValidationState.SUCCESS;
        }

        protected void ReleaseAll()
        {
            try
            {
                Monitor.Exit(resultNode);

            }
            catch (SynchronizationLockException ) { }
            try
            {
                if (parent != null)
                {
                    Monitor.Exit(parent);
                }
            }
            catch (SynchronizationLockException ) { }
            try
            {
                if (grandParent != null)
                {
                    Monitor.Exit(grandParent);
                }

            }
            catch (SynchronizationLockException ) { }

            ReleaseLockOnChildren();

        }

        protected bool AcquireLockOnChildren()
        {
            List<Node<T>> nodeList = resultNode.GetOutGoingEdgesList();
            if (!Monitor.TryEnter(nodeList)) return false;
            foreach (Node<T> node in nodeList)
            {
                if (!Monitor.TryEnter(node)) return false;
            }

            return true;
        }
        protected void ReleaseLockOnChildren()
        {
            List<Node<T>> nodeList = resultNode.GetOutGoingEdgesList();
            foreach (Node<T> node in nodeList)
            {
                try
                {
                    Monitor.Exit(node);
                }
                catch (SynchronizationLockException ) { }
            }
            try
            {
                Monitor.Exit(nodeList);
            }
            catch (SynchronizationLockException ) { }
        }

        public abstract T GetValueForKeyFound();

        public abstract IEnumerable<string> GetKeysWithGivenPrefix();

        public abstract IEnumerable<T> GetValuesOfKeysWithGivenPrefix();

        public abstract IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix();

        public abstract IEnumerable<string> GetKeysWithLongestGivenPrefix();

        public abstract IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix();

        public abstract IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix();

        public abstract ValidationState InsertSearchEntities(bool overwrite, T value, ref T res);

        public abstract ValidationState RemoveSearchEntities();

    }
}
