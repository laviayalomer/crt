﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixTree
{
    public class SearchEntities_PartOfKeyIsPartialToResult<T> : SearchEntities<T>
    {

        public SearchEntities_PartOfKeyIsPartialToResult(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {

        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<string>();
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            // Example: if we searched for CX, but deepest matching node was CO,
            // the results should include node CO and its descendants...
            string keyOfParentNode = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars - numOfEqualNodeChars);
            string keyOfNodeFound = RadixTreeUtils<T>.Concatenate(keyOfParentNode, resultNode.GetInEdges()).ToString();
            return currentTree.GetDescendantKeys(keyOfNodeFound, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
        }
        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {
            // Example: if we searched for CX, but deepest matching node was CO,
            // the results should include node CO and its descendants...
            string keyOfParentNode = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars - numOfEqualNodeChars);
            string keyOfNodeFound = RadixTreeUtils<T>.Concatenate(keyOfParentNode, resultNode.GetInEdges()).ToString();
            return currentTree.GetDescendantKeyValuePairs(keyOfNodeFound, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return default(T);
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<T>();
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {
            // Example: if we searched for CX, but deepest matching node was CO,
            // the results should include node CO and its descendants...
            string keyOfParentNode = RadixTreeUtils<T>.GetPrefixFromString(key, totalNumOfEqualChars - numOfEqualNodeChars);
            string keyOfNodeFound = RadixTreeUtils<T>.Concatenate(keyOfParentNode, resultNode.GetInEdges()).ToString();
            return currentTree.GetDescendantValues(keyOfNodeFound, resultNode);
        }

        public override T Insert(bool overwrite, T value)
        {
            // Search found a difference in characters between the key and the characters in the middle of the
            // edge in the current node, and the key still has trailing unmatched characters.
            // -> Split the node in three:
            // Let's call node found: NF
            // (1) Create a new node N1 containing the unmatched characters from the rest of the key, and the
            // value supplied to this method
            // (2) Create a new node N2 containing the unmatched characters from the rest of the edge in NF, and
            // copy the original edges and the value from NF unmodified into N2
            // (3) Create a new node N3, which will be the split node, containing the matched characters from
            // the key and the edge, and add N1 and N2 as child nodes of N3
            // (4) Re-add N3 to the parent node of NF, effectively replacing NF in the tree

            string keyCharsFromStartOfNodeFound = key.Substring(totalNumOfEqualChars - numOfEqualNodeChars);
            string commonPrefix = RadixTreeUtils<T>.GetCommonPrefix(keyCharsFromStartOfNodeFound, resultNode.GetInEdges());
            string suffixFromExistingEdge = RadixTreeUtils<T>.RemovePrefix(resultNode.GetInEdges(), commonPrefix);
            string suffixFromKey = key.Substring(totalNumOfEqualChars);

            // Create new nodes...
            Node<T> n1 = new Node<T>(value, suffixFromKey, new List<Node<T>>(), false);
            Node<T> n2 = new Node<T>(resultNode.GetValue(), suffixFromExistingEdge, resultNode.GetOutEdgesList(), false);
            List<Node<T>> newl = new List<Node<T>>();
            newl.Add(n1);
            newl.Add(n2);
            Node<T> n3 = new Node<T>(default(T), commonPrefix, newl, false);

            parent.UpdateOutEdgeByChildNode(n3);

            // Return null for the existing value...
            return default(T);
        }
        public override bool Remove()
        {
            return false;
        }
    }
}
