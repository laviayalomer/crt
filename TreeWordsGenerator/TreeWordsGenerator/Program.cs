﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeWordsGenerator
{
    public class TreeWordsGenerator
    {

        public static void Main(string[] args)
        {

        }
        public static void GenerateTextFileForDesiredTree(string path, int totalNumOfKeys, int treeDepth, ref int numOfChildren)
        {
            numOfChildren = CalculateNumOfChildrenS(totalNumOfKeys, treeDepth);
            List<string>[] arr = GenerateAllWordsLists(numOfChildren, totalNumOfKeys, treeDepth);

            using (TextWriter tw = new StreamWriter(path))
            {
                foreach (List<string> lst in arr)
                {
                    if (lst == null) return;
                    foreach (string s in lst)
                    {
                        tw.WriteLine(s);
                    }
                }
            }

        }


        private static List<string>[] GenerateAllWordsLists(int numOfChildren, int totalNumOfKeys, int treeDepth)
        {
            int currNumOfKeys = numOfChildren;
            int currentLength = 2;
            List<string>[] wordsOfLengthI = new List<string>[1000];
            wordsOfLengthI[0] = GetWordsOfLengthN(currentLength, numOfChildren, null, totalNumOfKeys);
            while (currNumOfKeys < totalNumOfKeys)
            {
               for (int i=1; i < currentLength; i++)
                {
                    wordsOfLengthI[i]=GetWordsOfLengthN(currentLength, numOfChildren, wordsOfLengthI[i-1], totalNumOfKeys - currNumOfKeys);
                    currentLength++;
                    if (currNumOfKeys > totalNumOfKeys) break;
                    currNumOfKeys += wordsOfLengthI[i].Count();
                }
            }
            return wordsOfLengthI;
        }

        private static List<string> GetWordsOfLengthN(int currentLength, int numOfChildren, List<string> baseList, int max)
        {
            string chars = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?ABCDEFGHIJKLMNOPQRSTUVWXYZ^&";
            Random rand = new Random();
            int rndc;
            string newS;
            List<string> returned = new List<string>();
            List<char> seenChars = new List<char>();
            if (baseList == null)
            {
                for (int i=0; i<numOfChildren; i++)
                {
                    rndc = rand.Next(0, chars.Length - 1); 
                    returned.Add(chars[rndc].ToString());
                    if (returned.Count > max) return returned;
                }

                return returned;
            }

            for (int i = 0; i < baseList.Count; i++)
            {
                seenChars = new List<char>();
                for (int j = 0; j < numOfChildren; j++)
                {
                    rndc = rand.Next(0, chars.Length - 1);
                    if (seenChars.Contains(chars[rndc]))
                    {
                        j--;
                        continue;
                    }

                    seenChars.Add(chars[rndc]);
                    newS = baseList[i] + chars[rndc].ToString();

                    returned.Add(newS);
                    if (returned.Count > max) return returned;
                }
            }

            return returned; 

        }

        public static int CalculateNumOfChildren(int numberOfKeys, int treeDepth)
        {
            int logbase = 2;
            int newLogbase = 3;
            double oldRes = 0, newRes = 0;
            while (true)
            {
                oldRes = Math.Log(numberOfKeys, logbase);
                newRes = Math.Log(numberOfKeys, newLogbase);
                if (oldRes > treeDepth && newRes < treeDepth)
                {
                    return (int)Math.Floor(oldRes);
                }

                if (oldRes < treeDepth)
                {
                    return (int)Math.Floor(oldRes);
                }
                logbase++;
                newLogbase++;
            }
        }
        public static int CalculateNumOfChildrenS(int numberOfKeys, int treeDepth)
        {
            int logbase = 2;
            int newLogbase = 3;
            double oldRes = 0, newRes = 0;
            while (true)
            {
                oldRes = Math.Log(numberOfKeys, logbase);
                newRes = Math.Log(numberOfKeys, newLogbase);
                if (oldRes > treeDepth && newRes < treeDepth)
                {
                    return logbase;
                }

                if (oldRes < treeDepth)
                {
                    return logbase;
                }
                logbase++;
                newLogbase++;
            }
        }

    }
}
