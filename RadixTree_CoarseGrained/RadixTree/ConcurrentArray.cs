﻿using System;
using System.Text;

namespace RadixTree
{
    public class ConcurrentArray<T>
    {
        private Object locker = new Object();
        private T[] array;
        public int size = 0;

        public ConcurrentArray(int size)
        {
            this.size = size;
            array = new T[size];
        }

        public ConcurrentArray(T[] arr)
        {
            int i = 0;
            size = arr.Length;
            array = new T[size];
            foreach (T element in arr)
            {
                array[i] = element;
                i++;
            }
        }

        public T[] ToArray()
        {
            return (T[])array.Clone();
        }

        public T Get(int slot)
        {
            return array[slot];
        }

        public bool ConcurrentSet(int slot, T update)
        {
            lock (locker)
            {
                array[slot] = update;
                return true;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[{" + array[0] + "}");
            for(int i=1;i<array.Length;i++)
            {
                sb.Append(", {" + array[i] + "}");
            }
            return sb.ToString();
        }
    }

}
