﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RadixTree_FineGrained
{
    public class SearchEntities_KeyIsPartialToResult<T> : SearchEntities<T>
    {
        public SearchEntities_KeyIsPartialToResult(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {
        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {            
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetIncomingEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.SearchChildernKeys(key, resultNode);
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetIncomingEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.SearchChildernKeys(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {            
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetIncomingEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.SearchChildernKeyValuePairs(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {            
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetIncomingEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.SearchChildernKeyValuePairs(key, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return default(T);
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {            
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetIncomingEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.SearchChildrenValues(key, resultNode);
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {            
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetIncomingEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.SearchChildrenValues(key, resultNode);
        }

        public override ValidationState InsertSearchEntities(bool overwrite, T value, ref T res)
        {
            if (!((grandParent == null ? true : Monitor.TryEnter(grandParent)) && (parent == null ? true : Monitor.TryEnter(parent)) && Monitor.TryEnter(resultNode)))
            {
                ReleaseAllI();
                return ValidationState.KEEP_GOING;
            }

            ValidationState validate = ValidateInsert();
            if (!validate.Equals(ValidationState.SUCCESS))
            {
                ReleaseAllI();
                return validate;
            }
            string keyCharsFromStartOfNodeFound = key.Substring(totalNumOfEqualChars - numOfEqualNodeChars);
            string commonPrefix = RadixTreeUtils<T>.GetCommonPrefix(keyCharsFromStartOfNodeFound, resultNode.GetIncomingEdges());
            string suffixFromExistingEdge = RadixTreeUtils<T>.RemovePrefix(resultNode.GetIncomingEdges(), commonPrefix);
            
            Node<T> newChild = new Node<T>(resultNode.GetValue(), suffixFromExistingEdge, resultNode.GetOutGoingEdgesList(), false);
            List<Node<T>> linkedNode = new List<Node<T>>();
            linkedNode.Add(newChild);
            Node<T> newParent = new Node<T>(value, commonPrefix, linkedNode, false);
            parent.UpdateOutEdgeByChildNode(newParent);

            ReleaseAllI();
            return ValidationState.SUCCESS;
        }

        public override ValidationState RemoveSearchEntities()
        {
            return ValidationState.FAILURE;
        }
    }
}
