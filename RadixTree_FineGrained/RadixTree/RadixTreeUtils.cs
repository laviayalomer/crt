﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace RadixTree
{
    class RadixTreeUtils<T>
    {
        public static string GetCommonPrefix(string first, string second)
        {
            int minLength = Math.Min(first.Length, second.Length);
            for (int i = 0; i < minLength; i++)
            {
                if (first[i] != second[i])
                {
                    return first.Substring(0, i);
                }
            }

            return first.Substring(0, minLength);
        }

        public static string GetSuffixFromString(string str, int startIndex)
        {
            if (startIndex >= str.Length)
            {
                return string.Empty;
            }

            return str.Substring(startIndex);
        }

        public static string GetPrefixFromString(string str, int endIndex)
        {
            if (endIndex > str.Length)
            {
                return str;
            }

            return str.Substring(0, endIndex);
        }
          
        public static string RemovePrefix(string str, string prefix)
        {
            if (prefix.Length > str.Length)
            {
                return string.Empty;
            }

            return str.Substring(prefix.Length);
        }

        public static StringBuilder Concatenate(string strA, string strB)
        {
            StringBuilder sbReturned = new StringBuilder();
            sbReturned.Append(strA);
            sbReturned.Append(strB);
            return sbReturned;
        }
    }

    public static class ListExtensions
    {
        public static Node<T>[] ListToArray<T>(this List<Node<T>> l)
        {
            if (l.GetType() == typeof(ConcurrentArrayAsList<Node<T>>))
            {
                return ((ConcurrentArrayAsList<Node<T>>)l).ToArray();
            }
            else
            {
                return l.ToArray();
            }
        }
        
        public static int Size<T>(this List<Node<T>> l)
        {
            if (l.GetType() == typeof(ConcurrentArrayAsList<Node<T>>))
            {
                return ((ConcurrentArrayAsList<Node<T>>)l).GetSize;
            }
            else
            {
                return l.Count;
            }
        }

        public static Node<T> GetIndex<T>(this List<Node<T>> l, int index)
        {
            if (l.GetType() == typeof(ConcurrentArrayAsList<Node<T>>))
            {
                return ((ConcurrentArrayAsList<Node<T>>)l)[index];
            }
            else
            {
                return l[index];
            }
        }
    }

    public static class LinkedListExtensions
    {
        public static void AddAll<T>(this LinkedList<Node<T>> ll, List<Node<T>> l)
        {
            for(int i = 0; i < l.Size(); i++)
            {
                ll.AddLast(l.GetIndex(i));
            }
        }

        public static void AddAll<T>(this List<Node<T>> ll, List<Node<T>> l)
        {
            for(int i = 0; i < l.Size(); i++)
            {
                ll.Add(l.GetIndex(i));
            }
        }
    }

    public class NodeKeyPair<T>
    {
        public readonly Node<T> node;
        public readonly string key;

        public NodeKeyPair(Node<T> node, string key)
        {
            this.node = node;
            this.key = key;
        }
    }
}