﻿using System;


namespace RadixTree_CoarseGrained
{
    public class SearchEntities_Factory<T>
    {
        private CRT<T> currentTree;
        public class InnerSearchResult
        {
            public InnerSearchResult(CRT<T> CurrentTree, Node<T> ResultNode, Node<T> Parent, Node<T> GrandParent, string Key, int NumOfEqualNodeChars, int TotalNumOfEqualChars)
            {
                this.ResultNode = ResultNode;
                this.Parent = Parent;
                this.GrandParent = GrandParent;
                this.Key = Key;
                this.NumOfEqualNodeChars = NumOfEqualNodeChars;
                this.TotalNumOfEqualChars = TotalNumOfEqualChars;
                this.CurrentTree = CurrentTree;
            }
            public CRT<T> CurrentTree
            {
                get;
                set;
            }
            public Node<T> ResultNode
            {
                get;
                set;
            }

            public Node<T> Parent
            {
                get;
                set;
            }

            public Node<T> GrandParent
            {
                get;
                set;
            }

            public string Key
            {
                get;
                set;
            }

            // Represents the number of equal chars from key in ResultNode
            public int NumOfEqualNodeChars
            {
                get;
                set;
            }

            // Represents the number of equal chars from key in branch
            public int TotalNumOfEqualChars
            {
                get;
                set;
            }
        }

        public SearchEntities_Factory(CRT<T> currentTree)
        {
            this.currentTree = currentTree;
        }
        public SearchEntities<T> CreateSearchEntity(string key)
        {
            InnerSearchResult res = Search(key);

            if (res.TotalNumOfEqualChars == key.Length)
            {
                if (res.NumOfEqualNodeChars == res.ResultNode.GetIncomingEdges().Length)
                {
                    return new SearchEntities_KeyFound<T>(res);
                }
                else if (res.NumOfEqualNodeChars < res.ResultNode.GetIncomingEdges().Length)
                {
                    return new SearchEntities_KeyIsPartialToResult<T>(res);
                }
            }
            else if (res.TotalNumOfEqualChars < key.Length)
            {
                if (res.NumOfEqualNodeChars == res.ResultNode.GetIncomingEdges().Length)
                {
                    return new SearchEntities_PartOfKeyFound<T>(res);
                }
                else if (res.NumOfEqualNodeChars < res.ResultNode.GetIncomingEdges().Length)
                {
                    return new SearchEntities_PartOfKeyIsPartialToResult<T>(res);
                }
            }

            throw new Exception("Invalid SearchEntitie: " + this);
        }

        private InnerSearchResult Search(string key)
        {
            Node<T> grandParent = null;
            Node<T> parent = null;
            Node<T> resultNode = currentTree.root;
            int totalNumOfEqualChars = 0, numOfEqualNodeChars = 0;

            int keyLength = key.Length;
            while (totalNumOfEqualChars < keyLength)
            {
                Node<T> nextNode = resultNode.GetOutEdgeByFirstChar(key[totalNumOfEqualChars]);
                if (nextNode == null)
                {
                    break;
                }

                grandParent = parent;
                parent = resultNode;
                resultNode = nextNode;
                numOfEqualNodeChars = 0;
                string currentNodeEdgeCharacters = resultNode.GetIncomingEdges();

                for (int i = 0, numEdgeChars = currentNodeEdgeCharacters.Length; i < numEdgeChars && totalNumOfEqualChars < keyLength; i++)
                {
                    if (currentNodeEdgeCharacters[i] != key[totalNumOfEqualChars])
                    {
                        return new InnerSearchResult(currentTree, resultNode, parent, grandParent, key, numOfEqualNodeChars, totalNumOfEqualChars);
                    }

                    totalNumOfEqualChars++;
                    numOfEqualNodeChars++;
                }
            }

            return new InnerSearchResult(currentTree, resultNode, parent, grandParent, key, numOfEqualNodeChars, totalNumOfEqualChars);
        }
    }
}