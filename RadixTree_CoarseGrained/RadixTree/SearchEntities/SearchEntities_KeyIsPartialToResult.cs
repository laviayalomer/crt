﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixTree
{
    public class SearchEntities_KeyIsPartialToResult<T> : SearchEntities<T>
    {
        public SearchEntities_KeyIsPartialToResult(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {

        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            // Append the remaining characters of the edge to the key.
            // For example if we searched for CO, but first matching node was COFFEE,
            // the key associated with the first node should be COFFEE...
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetInEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.GetDescendantKeys(key, resultNode);
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            // Append the remaining characters of the edge to the key.
            // For example if we searched for CO, but first matching node was COFFEE,
            // the key associated with the first node should be COFFEE...
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetInEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.GetDescendantKeys(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            // Append the remaining characters of the edge to the key.
            // For example if we searched for CO, but first matching node was COFFEE,
            // the key associated with the first node should be COFFEE...
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetInEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.GetDescendantKeyValuePairs(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {
            // Append the remaining characters of the edge to the key.
            // For example if we searched for CO, but first matching node was COFFEE,
            // the key associated with the first node should be COFFEE...
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetInEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.GetDescendantKeyValuePairs(key, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return default(T);
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            // Append the remaining characters of the edge to the key.
            // For example if we searched for CO, but first matching node was COFFEE,
            // the key associated with the first node should be COFFEE...
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetInEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.GetDescendantValues(key, resultNode);
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {
            // Append the remaining characters of the edge to the key.
            // For example if we searched for CO, but first matching node was COFFEE,
            // the key associated with the first node should be COFFEE...
            string edgeSuffix = RadixTreeUtils<T>.GetSuffixFromString(resultNode.GetInEdges(), numOfEqualNodeChars);
            key = RadixTreeUtils<T>.Concatenate(key, edgeSuffix).ToString();
            return currentTree.GetDescendantValues(key, resultNode);
        }

        public override T Insert(bool overwrite, T value)
        {
            // Search ran out of characters from the key while in the middle of an edge in the node.
            // -> Split the node in two: Create a new parent node storing the new value,
            // and a new child node holding the original value and edges from the existing node...
            string keyCharsFromStartOfNodeFound = key.Substring(totalNumOfEqualChars - numOfEqualNodeChars);
            string commonPrefix = RadixTreeUtils<T>.GetCommonPrefix(keyCharsFromStartOfNodeFound, resultNode.GetInEdges());
            string suffixFromExistingEdge = RadixTreeUtils<T>.RemovePrefix(resultNode.GetInEdges(), commonPrefix);

            // Create new nodes...
            Node<T> newChild = new Node<T>(resultNode.GetValue(), suffixFromExistingEdge, resultNode.GetOutEdgesList(), false);
            List<Node<T>> linkedNode = new List<Node<T>>();
            linkedNode.Add(newChild);
            Node<T> newParent = new Node<T>(value, commonPrefix, linkedNode, false);

            // Add the new parent to the parent of the node being replaced (replacing the existing node)...
            parent.UpdateOutEdgeByChildNode(newParent);

            // Return null for the existing value...
            return default(T);
        }

        public override bool Remove()
        {
            return false;
        }
    }
}
