﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixTree
{
    public class SearchEntities_Invalid<T> : SearchEntities<T>
    {
        public SearchEntities_Invalid(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {
            
        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<string>();
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<string>();
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<KeyValuePair<string, T>>();
        }

        public override T GetValueForKeyFound()
        {
            return default(T);
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<T>();
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {
            return new Enumerators<T>.EmptyBaseEnumerable<T>();
        }

        public override T Insert(bool overwrite, T value)
        {
            throw new Exception("Unexpected SearchEntity type");
        }
        public override bool Remove()
        {
            return false;
        }
    }
}
