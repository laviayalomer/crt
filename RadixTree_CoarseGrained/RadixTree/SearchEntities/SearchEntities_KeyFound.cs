﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixTree
{
    public class SearchEntities_KeyFound<T> : SearchEntities<T>
    {
        public SearchEntities_KeyFound(SearchEntities_Factory<T>.InnerSearchResult res) :
            base(res)
        {

        }

        public override IEnumerable<string> GetKeysWithGivenPrefix()
        {
            return currentTree.GetDescendantKeys(key, resultNode);
        }

        public override IEnumerable<string> GetKeysWithLongestGivenPrefix()
        {
            return currentTree.GetDescendantKeys(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithGivenPrefix()
        {
            return currentTree.GetDescendantKeyValuePairs(key, resultNode);
        }

        public override IEnumerable<KeyValuePair<string, T>> GetKeyValuePairsOfKeysWithLongestGivenPrefix()
        {
            return currentTree.GetDescendantKeyValuePairs(key, resultNode);
        }

        public override T GetValueForKeyFound()
        {
            return resultNode.GetValue();
        }

        public override IEnumerable<T> GetValuesOfKeysWithGivenPrefix()
        {
            return currentTree.GetDescendantValues(key, resultNode);
        }

        public override IEnumerable<T> GetValuesOfKeysWithLongestGivenPrefix()
        {
            return currentTree.GetDescendantValues(key, resultNode);
        }

        public override T Insert(bool overwrite, T value)
        {
            // Search found an exact match for all edges leading to this node.
            // -> Add or update the value in the node found, by replacing
            // the existing node with a new node containing the value...

            // First check if existing node has a value, and if we are allowed to overwrite it.
            // Return early without overwriting if necessary...
            T existingValue = resultNode.GetValue();
            if (!overwrite && existingValue != null)
            {
                return existingValue;
            }
            // Create a replacement for the existing node containing the new value...
            Node<T> replacementNode = new Node<T>(value, resultNode.GetInEdges(), resultNode.GetOutEdgesList(), false);
            parent.UpdateOutEdgeByChildNode(replacementNode);
            // Return the existing value...
            return existingValue;
        }

        public override bool Remove()
        {
            if (resultNode.GetValue().Equals(default(T)))
            {
                // This node was created automatically as a split between two branches (implicit node).
                // No need to remove it...
                return false;
            }

            // Proceed with deleting the node...
            List<Node<T>> childEdges = resultNode.GetOutEdgesList();
            if (childEdges.Size() > 1)
            {
                // This node has more than one child, so if we delete the value from this node, we still need
                // to leave a similar node in place to act as the split between the child edges.
                // Just delete the value associated with this node.
                // -> Clone this node without its value, preserving its child nodes...
                Node<T> cloned = new Node<T>(default(T), resultNode.GetInEdges(), resultNode.GetOutEdgesList(), false);
                // Re-add the replacement node to the parent...
                parent.UpdateOutEdgeByChildNode(cloned);
            }
            else if (childEdges.Size() == 1)
            {
                // Node<T> has one child edge.
                // Create a new node which is the concatenation of the edges from this node and its child,
                // and which has the outgoing edges of the child and the value from the child.
                Node<T> child = childEdges.GetIndex(0);
                string concatenatedEdges = RadixTreeUtils<T>.Concatenate(resultNode.GetInEdges(), child.GetInEdges()).ToString();
                Node<T> mergedNode = new Node<T>(child.GetValue(), concatenatedEdges, child.GetOutEdgesList(), false);
                // Re-add the merged node to the parent...
                parent.UpdateOutEdgeByChildNode(mergedNode);
            }
            else
            {
                // Node<T> has no children. Delete this node from its parent,
                // which involves re-creating the parent rather than simply updating its child edge
                // (this is why we need parentNodesParent).
                // However if this would leave the parent with only one remaining child edge,
                // and the parent itself has no value (is a split node), and the parent is not the root node
                // (a special case which we never merge), then we also need to merge the parent with its
                // remaining child.

                List<Node<T>> currentEdgesFromParent = parent.GetOutEdgesList();
                // Create a list of the outgoing edges of the parent which will remain
                // if we remove this child...
                // Use a non-resizable list, as a sanity check to force ArrayIndexOutOfBounds...
                List<Node<T>> newEdgesOfParent = new List<Node<T>>();
                int numParentEdges = currentEdgesFromParent.Size();
                for (int i = 0; i < numParentEdges; i++)
                {
                    Node<T> node = currentEdgesFromParent.GetIndex(i);
                    if (node != resultNode)
                    {
                        newEdgesOfParent.Add(node);
                    }
                }

                // Note the parent might actually be the root node (which we should never merge)...
                bool parentIsRoot = false;
                if(parent == currentTree.root)
                {
                    parentIsRoot = true;
                }
                
                Node<T> newParent;
                if (newEdgesOfParent.Size() == 1 && parent.GetValue().Equals(default(T)) && !parentIsRoot)
                {
                    // Parent is a non-root split node with only one remaining child, which can now be merged.
                    Node<T> parentsRemainingChild = newEdgesOfParent.GetIndex(0);
                    // Merge the parent with its only remaining child...
                    string concatenatedEdges = RadixTreeUtils<T>.Concatenate(parent.GetInEdges(), parentsRemainingChild.GetInEdges()).ToString();
                    newParent = new Node<T>(parentsRemainingChild.GetValue(), concatenatedEdges, parentsRemainingChild.GetOutEdgesList(), parentIsRoot);
                }
                else
                {
                    // Parent is a node which either has a value of its own, has more than one remaining
                    // child, or is actually the root node (we never merge the root node).
                    // Create new parent node which is the same as is currently just without the edge to the
                    // node being deleted...
                    newParent = new Node<T>(parent.GetValue(), parent.GetInEdges(), newEdgesOfParent, parentIsRoot);
                }
                // Re-add the parent node to its parent...
                if (parentIsRoot)
                {
                    // Replace the root node...
                    currentTree.root = newParent;
                }
                else
                {
                    // Re-add the parent node to its parent...
                    grandParent.UpdateOutEdgeByChildNode(newParent);
                }
            }

            return true;
        }
    }
}
